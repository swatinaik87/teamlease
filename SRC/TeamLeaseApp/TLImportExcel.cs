﻿using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace TeamLeaseApp
{
    public class TLImportExcel
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //string[] fileEntries = Directory.GetFiles(targetDirectory);
        //foreach (string fileName in fileEntries)
        //    ProcessFile(fileName);     

        public void ExcelInsertToDB()
        {
            try
            {
                //string FilePath = "D:\\Download";
                string FilePath = System.Configuration.ConfigurationManager.AppSettings["FilePath"].ToString();
                string[] files = Directory.GetFiles(FilePath);
                foreach (string fileName in files)
                {
                    if (ValidateExcelFile(fileName))
                    {
                        ////InsertAuditLogWithIP(AdminUserName, "EXCEL_IMPORT", 1, 0);
                        string FileName = Path.GetFileName(fileName);
                        string Extension = Path.GetExtension(fileName);
                        //string enrollFrom = "2018-07-27";
                        //string enrollTo = "2018-07-27";
                        string enrollFrom = DateTime.Now.ToString("yyyy-MM-dd");
                        string enrollTo = DateTime.Now.ToString("yyyy-MM-dd");

                        //DateTime.Parse("5/13/2012");
                        //log.Info("saving(uploading to excelfile folder) excel file.please check time.");
                        //string FilePath = Server.MapPath("ExcelFiles/" + FileName);
                        //fupEnroll.SaveAs(FilePath);
                        //log.Info("saving(uploading to excelfile folder) excel file completed.please check time.");
                        string errMessage = string.Empty;
                        ValidateExcel obj = new ValidateExcel();
                       long lotNo= obj.ToExcel(fileName, Extension, "10125", enrollFrom, enrollTo, ref errMessage);

                        if (lotNo>0)
                        {
                            log.Info("successfully uploaded");
                        }
                        else
                        {
                            log.Info("Error in uploading excel.");

                        }
                        //if (File.Exists(FilePath))
                        //{
                        //    File.Delete(FilePath);
                        //}

                    //Commented by annapurna, need to uncomment
                        //foreach (string fileName1 in files)
                        //{
                        //    File.Delete(fileName1);
                        //}
                        //annapurna


                        //long lotNo = 0;
                        //string errMessage = string.Empty;
                        //Session["LotCompId"] = ddlCompany.SelectedValue;
                        //lotNo = obj.ToExcel(FilePath, Extension, ddlCompany.SelectedValue, txtEnrollFrom.Text.Trim(), txtEnrollTo.Text.Trim(), ref errMessage);
                        //if (string.IsNullOrEmpty(errMessage))
                        //{
                        //    log.Info("successfully uploaded");
                        //    log.Info("LotNO generated :" + lotNo);
                        //    lblMessage.Text = "Lot No. " + lotNo.ToString();
                        //    ViewState["lotNo"] = lotNo;
                        //    if (lotNo > 0)
                        //    {
                        //        lblValidationMsg.Text = "Data validations on excel completed.Please click on 'Next' to complete the upload process.";
                        //        trUpload.Visible = false;

                        //        trNext.Visible = true;
                        //    }
                        //}
                        //else
                        //{
                        //    log.Info("Error in uploading excel.");
                        //    lblValidationMsg.Text = errMessage;
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in ExcelInsertToDB" + ex.Message);
            }

        }



        // <summary>
        // Input fields validation method and check is a valid excel file.
        // </summary>
        // <returns></returns>
        public bool ValidateExcelFile(string FileName)
        {
            bool isValidFile = false;
            try
            {
                string[] validFileTypes = { "xls", "xlsx" };
                string ext = System.IO.Path.GetExtension(FileName);
                for (int i = 0; i < validFileTypes.Length; i++)
                {
                    if (ext == "." + validFileTypes[i])
                    {
                        isValidFile = true;
                        break;
                    }
                }
                if (!isValidFile)
                {
                    log.Error("Invalid file");
                }
            }
            catch (Exception ex)
            {
                isValidFile = false;
                log.Error("ValidateExcelFile" + ex.Message);
            }
            return isValidFile;
        }


        //string _ftpURL = "ftp://192.168.100.123";           //Host URL or address of the FTP server
        //string _UserName = "ftpuser";                 //User Name of the FTP server
        //string _Password = "ftpuser@123";              //Password of the FTP server
        //string _ftpDirectory = "harddisk2/ftp_share";          //The directory in FTP server where the files are present
        //string _FileName = "United_Format_FTP.xlsx";             //File name, which one will be downloaded
        //string _LocalDirectory = "D:\\Download";  //Local directory where the files will be downloaded

        string _ftpURL = System.Configuration.ConfigurationManager.AppSettings["_ftpURL"].ToString();
        string _UserName = System.Configuration.ConfigurationManager.AppSettings["_UserName"].ToString();
        string _Password = System.Configuration.ConfigurationManager.AppSettings["_Password"].ToString();
        string _ftpDirectory = System.Configuration.ConfigurationManager.AppSettings["_ftpDirectory"].ToString();
        string _FileName = System.Configuration.ConfigurationManager.AppSettings["_FileName"].ToString();
        string _LocalDirectory = System.Configuration.ConfigurationManager.AppSettings["_LocalDirectory"].ToString();

        //public void DownloadFile(string ftpURL, string UserName, string Password, string ftpDirectory, string FileName, string LocalDirectory)

        public void DownloadFile()
        {
            //if (!File.Exists(LocalDirectory + "/" + FileName)) //Commented by annapurna
            //{

                try
                {

                    if (!Directory.Exists(_LocalDirectory))
                    {
                        Directory.CreateDirectory(_LocalDirectory);
                    }
        
                //downloading and reding file
                //FtpWebRequest requestFileDownload = (FtpWebRequest)WebRequest.Create(_ftpURL + "/" + _ftpDirectory + "/" + _FileName);
                FtpWebRequest requestFileDownload = (FtpWebRequest)WebRequest.Create(_ftpURL  + "/" + _FileName);
                requestFileDownload.Credentials = new NetworkCredential(_UserName, _Password);
             
                    requestFileDownload.Method = WebRequestMethods.Ftp.DownloadFile;
                    FtpWebResponse responseFileDownload = (FtpWebResponse)requestFileDownload.GetResponse();
                    Stream responseStream = responseFileDownload.GetResponseStream();
                    //FileStream writeStream = new FileStream(LocalDirectory + "/" + FileName, FileMode.Create);//commented by annapurna
                    FileStream writeStream = new FileStream(_LocalDirectory + "/" + _FileName, FileMode.Create);
                    int Length = 1048576 * 1000;
                    Byte[] buffer = new Byte[Length];

                    int bytesRead = responseStream.Read(buffer, 0, Length);
                    while (bytesRead > 0)
                    {
                        writeStream.Write(buffer, 0, bytesRead);
                        bytesRead = responseStream.Read(buffer, 0, Length);
                    }
                    responseStream.Close();
                    writeStream.Close();
                    requestFileDownload = null;

                    //Deleting a file from the Server
                    //DeleteFile(ftpURL, UserName, Password, ftpDirectory, FileName); //commented by annapurna
                    DeleteFile(_ftpURL, _UserName, _Password, _ftpDirectory, _FileName);

                    ExcelInsertToDB();
                   
                }
                catch (Exception ex)
                {
                    throw ex;
                }
          //  }
        }

        public void DeleteFile(string _ftpURL, string _UserName, string _Password, string _ftpDirectory, string _FileName)
        {
            FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(_ftpURL + "/" +_FileName);
            ftpRequest.Credentials = new NetworkCredential(_UserName, _Password);
            ftpRequest.Method = WebRequestMethods.Ftp.DeleteFile;
            FtpWebResponse responseFileDelete = (FtpWebResponse)ftpRequest.GetResponse();

            // FtpWebRequest requestFileDownload = (FtpWebRequest)WebRequest.Create(_ftpURL + "/" + _ftpDirectory + "/" + _FileName);
            // requestFileDownload.Credentials = new NetworkCredential(_UserName, _Password);
            // requestFileDownload.Method = WebRequestMethods.Ftp.DeleteFile;
            //// FtpWebResponse responseFileDownload = (FtpWebResponse)requestFileDownload.GetResponse();

            // Stream responseStream = responseFileDownload.GetResponseStream();
        }

        public void DownloadFiles()
        {
            FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(_ftpURL);
            ftpRequest.Credentials = new NetworkCredential(_UserName, _Password);
            ftpRequest.Method = WebRequestMethods.Ftp.ListDirectory;
            FtpWebResponse response = (FtpWebResponse)ftpRequest.GetResponse();
            StreamReader streamReader = new StreamReader(response.GetResponseStream());
            List<string> directories = new List<string>();

            string line = streamReader.ReadLine();
            while (!string.IsNullOrEmpty(line))
            {
                directories.Add(line);
                line = streamReader.ReadLine();
            }
            streamReader.Close();


            using (WebClient ftpClient = new WebClient())
            {
                ftpClient.Credentials = new System.Net.NetworkCredential(_UserName, _Password);

                for (int i = 0; i <= directories.Count - 1; i++)
                {
                    if (directories[i].Contains("."))
                    {
                        string FilePath = System.Configuration.ConfigurationManager.AppSettings["FilePath"].ToString();
                        string path = _ftpURL + directories[i].ToString();
                        //string trnsfrpth = @"D:\\Download\" + directories[i].ToString();
                        string trnsfrpth = FilePath + directories[i].ToString();
                        if (!File.Exists(trnsfrpth))
                        {
                            ftpClient.DownloadFile(path, trnsfrpth);
                        }
                       // FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(_ftpURL + "/" + _ftpDirectory + "/" + _FileName);
                        //ftpRequest.Credentials = new NetworkCredential(_UserName, _Password);
                        FtpWebRequest ftpRequest1 = (FtpWebRequest)WebRequest.Create(path);
                        ftpRequest1.Credentials = new NetworkCredential(_UserName, _Password);
                        ftpRequest1.Method = WebRequestMethods.Ftp.DeleteFile;
                        FtpWebResponse responseFileDelete = (FtpWebResponse)ftpRequest1.GetResponse();
                        

                    }
                }
            }
            ExcelInsertToDB();

        }


    }
}
