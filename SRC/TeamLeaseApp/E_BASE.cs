﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace TeamLeaseApp
{
    public class E_BASE
    {
        public E_BASE()
        {
            Message = new StringBuilder();
        }

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static Regex regxAplha = new Regex("^[a-zA-Z0-9 &]+$");
        public static Regex regxDate = new Regex("^(3[01]|[12][0-9]|0[1-9])-(1[0-2]|0[1-9])-[0-9]{4}$");
       // public static Regex regxDate = new Regex("^(3[01]|[12][0-9]|0[1-9])-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-[0-9]{2}$");
      //  public static Regex regxDateRSBY = new Regex("^([0]?[0-9]|[12][0-9]|[3][01])[./-]([0]?[1-9]|[1][0-2])[./-]([0-9]{4}|[0-9]{2})$");

        public static Regex regxDateRSBY = new Regex("^([0]?[1-9]|[1][0-2])[./-]([0]?[0-9]|[12][0-9]|[3][01])[./-]([0-9]{4}|[0-9]{2})$");
        public static Regex regxNumeric = new Regex("^[0-9]*$");
       // public static Regex regxVoda = new Regex("^[a-z0-9._]+@vodafone.com$");



        public int RowNo { get; set; }

        public StringBuilder Message { get; set; }


        public string MaxLength(string value, string propertyName, int length)
        {
            try
            {
              
                 if (!string.IsNullOrEmpty(value) && value.Length > length)
                {
                    Message.Append(propertyName + " Length cannot excced more than " + length + ".Check RowNo: " + RowNo + "<br>");
                    value = null;
                }
            }
            catch (Exception ex)
            {
                value = null;
                log.Error("Error in Illegal. " + propertyName + " " + ex.Message);
            }
            return value;
        }

        public string Illegal(string value, string propertyName, int length)
        {
            try
            {
                if (!string.IsNullOrEmpty(value) && !regxAplha.IsMatch(value))
                {
                    Message.Append("Illegal characters in " + propertyName + " column.Check RowNo: " + RowNo + "<br>");
                    value = null;
                }
                else if (!string.IsNullOrEmpty(value) && value.Length > length)
                {
                    Message.Append(propertyName + " Length cannot excced more than " + length + ".Check RowNo: " + RowNo + "<br>");
                    value = null;
                }
            }
            catch (Exception ex)
            {
                value = null;
                log.Error("Error in Illegal. " + propertyName + " " + ex.Message);
            }
            return value;
        }

        public string IllegalMand(string value, string propertyName, int length, string ein = null)
        {
            try
            {
                string refEIN = string.IsNullOrEmpty(ein) ? string.Empty : "Ref EIN." + ein + " ";
                if (string.IsNullOrEmpty(value))
                {
                    Message.Append(refEIN + "" + propertyName + " is Mandatory.Check RowNo: " + RowNo + "<br>");
                    value = null;
                }
                //else if (!string.IsNullOrEmpty(value) && !regxAplha.IsMatch(value))
                //{
                //    Message.Append(refEIN + "" + "Illegal characters in " + propertyName + " column.Check RowNo: " + RowNo + "<br>");
                //    value = null;// commented by annapurna
                //    //value = "Ingersoll rand India Limited";
                //}
                else if (!string.IsNullOrEmpty(value) && value.Length > length)
                {
                    Message.Append(refEIN + "" + propertyName + " Length cannot excced more than " + length + ".Check RowNo: " + RowNo + "<br>");
                    value = null;
                }
            }
            catch (Exception ex)
            {
                value = null;
                log.Error("Error in Illegal. " + propertyName + " " + ex.Message);
            }
            return value;
        }

        public string NumericIllegalMand(string value, string propertyName)
        {
            try
            {
                if (string.IsNullOrEmpty(value))
                {
                    Message.Append(propertyName + " is Mandatory.Check RowNo: " + RowNo + "<br>");
                    value = "0";
                }
                else if (!string.IsNullOrEmpty(value) && !regxNumeric.IsMatch(value))
                {
                    Message.Append(propertyName + " column must be numeric.Check RowNo: " + RowNo + "<br>");
                    value = "0";
                }
                else if (!string.IsNullOrEmpty(value) && Convert.ToInt64(value) > 2147483647)
                {
                    Message.Append(propertyName + " max length exceeds.Check RowNo: " + RowNo + "<br>");
                    value = "0";
                }
            }
            catch (Exception ex)
            {
                value = "0";
                log.Error("Error in Illegal. " + propertyName + " " + ex.Message);
            }
            return value;
        }

        public string NumericIllegal(string value, string propertyName)
        {
            try
            {
                if (string.IsNullOrEmpty(value))
                {
                    value = "0";
                }
                else if (!string.IsNullOrEmpty(value) && !regxNumeric.IsMatch(value))
                {
                    Message.Append(propertyName + " name must be numeric.Check RowNo: " + RowNo + "<br>");
                    value = "0";
                }
                else if (!string.IsNullOrEmpty(value) && Convert.ToInt64(value) > 2147483647)
                {
                    Message.Append(propertyName + " max length exceeds.Check RowNo: " + RowNo + "<br>");
                    value = "0";
                }
            }
            catch (Exception ex)
            {
                value = "0";
                log.Error("Error in Illegal. " + propertyName + " " + ex.Message);
            }
            return value;
        }

        public string InvalidDate(string value, string propertyName, string ein = null)
        {
            //  var date = date(value);
            string value1 = string.Empty;
            if (value != null)
            {

                 value1 = value.Replace("12:00:00 AM", "").Trim();
                // value= DateTime.ParseExact(value, "dd-mm-yyyy", CultureInfo.InvariantCulture).ToString();
                string refEIN = string.Empty;
                try
                {
                    refEIN = string.IsNullOrEmpty(ein) ? string.Empty : "Ref EIN." + ein + " ";
                    if (string.IsNullOrEmpty(value1))
                    {
                        Message.Append(refEIN + "" + "Mandatory " + propertyName + " column.Check RowNo: " + RowNo + "<br>");
                        value1 = null;
                    }
                    //else if (!string.IsNullOrEmpty(value))
                    //{
                    //    value = SetISOFormat(value);
                    //}
                    else if (!string.IsNullOrEmpty(value1) && !regxDateRSBY.IsMatch(value1))
                    {
                        Message.Append(refEIN + "" + "Invalid Date.Date format must be (MM/dd/yyyy) " + propertyName + " column.Check RowNo: " + RowNo + "<br>");
                        value1 = null;
                    }
                    else if (!string.IsNullOrEmpty(value1) && regxDateRSBY.IsMatch(value1))
                    {
                        value1 = value1.Split('/')[2] + "-" + value1.Split('/')[0] + "-" + value1.Split('/')[1];
                    }
                }
                catch (Exception ex)
                {
                    Message.Append(refEIN + "" + propertyName + " " + ex.Message + "<br>");
                    value = null;
                    log.Error("Error in Illegal. " + propertyName + " " + ex.Message);
                }
            }
            return value1;
        }

        public string NmIllegalDec(string value, string propertyName)
        {
            try
            {
                if (string.IsNullOrEmpty(value))
                    value = "0";
                if (!string.IsNullOrEmpty(value) && !regxNumeric.IsMatch(value))
                {
                    Message.Append(propertyName + " name must be numeric.Check RowNo: " + RowNo + "<br>");
                    value = "0";
                }
                else if (!string.IsNullOrEmpty(value) && Convert.ToDouble(value) > 1000000)
                {
                    Message.Append(propertyName + " max length exceeds.Check RowNo: " + RowNo + "<br>");
                    value = "0";
                }
            }
            catch (Exception ex)
            {
                value = "0";
                log.Error("Error in Illegal. " + propertyName + " " + ex.Message);
            }
            return value;
        }

        //public string IsValidEmail(string value, string propertyName, int compId)
        //{
        //    try
        //    {
        //        string band = System.Web.HttpContext.Current.Session["band"] != null ? System.Web.HttpContext.Current.Session["band"].ToString() : string.Empty; ;
        //        if (string.IsNullOrEmpty(value))
        //        {
        //            Message.Append(propertyName + " Mandatory.Check RowNo: " + RowNo + "<br>");
        //            value = string.Empty;
        //        }
        //        else if (!string.IsNullOrEmpty(value))
        //        {
        //            if (band == "VODAFONE")
        //            {
        //                if (!regxVoda.IsMatch(value.Trim()))
        //                {
        //                    Message.Append(propertyName + " Invalid.Domain must be @vodafone.com.Check RowNo: " + RowNo + "<br>");
        //                    value = string.Empty;
        //                }
        //            }
        //            else if (band != "VODAFONE")
        //            {
        //                if (!Regex.IsMatch(value.Trim(), @"\A(?:[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?)\Z"))
        //                {
        //                    Message.Append(propertyName + " Invalid.Check RowNo: " + RowNo + "<br>");
        //                    value = string.Empty;
        //                }
        //            }
        //            else
        //                Message.Append("we are unable to validate email.please contact administrator.<br>");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        value = string.Empty;
        //        log.Error("Error in IsValidEmail. " + propertyName + " " + ex.Message);
        //    }
        //    return value.Trim();
        //}

        //public string IllegalBand(string value, string propertyName, int length, int compId)
        //{
        //    try
        //    {
        //        string band = System.Web.HttpContext.Current.Session["band"] != null ? System.Web.HttpContext.Current.Session["band"].ToString() : string.Empty;
        //        if (string.IsNullOrEmpty(value))
        //        {
        //            Message.Append(propertyName + " is Mandatory.Check RowNo: " + RowNo + "<br>");
        //            value = null;
        //        }
        //        else if (!string.IsNullOrEmpty(value) && !regxAplha.IsMatch(value))
        //        {
        //            Message.Append("Illegal characters in " + propertyName + " column.Check RowNo: " + RowNo + "<br>");
        //            value = null;
        //        }
        //        else if (!string.IsNullOrEmpty(value) && value.Length > length)
        //        {
        //            Message.Append(propertyName + " Length cannot excced more than " + length + ".Check RowNo: " + RowNo + "<br>");
        //            value = null;
        //        }
        //        //else if (band.Trim() != value.Trim())
        //        //{
        //        //    Message.Append(propertyName + " must be '" + band + "'.Check RowNo: " + RowNo + "<br>");
        //        //    value = null;
        //        //}
        //        //else if (compId == 4 && value != "FAI")
        //        //{
        //        //    Message.Append(propertyName + " must be 'FAI'.Check RowNo: " + RowNo + "<br>");
        //        //    value = null;
        //        //}
        //        //else if (compId == 5 && value != "PACE")
        //        //{
        //        //    Message.Append(propertyName + " must be 'PACE'.Check RowNo: " + RowNo + "<br>");
        //        //    value = null;
        //        //}
        //        //else if (compId == 10010 && value != "LOWES")
        //        //{
        //        //    Message.Append(propertyName + " must be 'LOWES'.Check RowNo: " + RowNo + "<br>");
        //        //    value = null;
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        value = null;
        //        log.Error("Error in Illegal. " + propertyName + " " + ex.Message);
        //    }
        //////    return value;
        //}

        public string InvalidDateFormat(string value, string propertyName, string ein = null)
        {
            string refEIN = string.Empty;
            try
            {
                refEIN = string.IsNullOrEmpty(ein) ? string.Empty : "Ref EIN." + ein + " ";
                if (string.IsNullOrEmpty(value))
                {
                    Message.Append(refEIN + "" + "Mandatory " + propertyName + " column.Check RowNo: " + RowNo + "<br>");
                    value = null;
                }
                //else if (!string.IsNullOrEmpty(value))
                //{
                //    value = SetISOFormat(value);
                //}
                else if (!string.IsNullOrEmpty(value) && !regxDateRSBY.IsMatch(value))
                {
                    Message.Append(refEIN + "" + "Invalid Date.Date format must be (dd/mm/yyyy) " + propertyName + " column.Check RowNo: " + RowNo + "<br>");
                    value = null;
                }
                else if (!string.IsNullOrEmpty(value) && regxDate.IsMatch(value))
                {
                    value = value.Split('-')[2] + "-" + value.Split('-')[1] + "-" + value.Split('-')[0];
                }
            }
            catch (Exception ex)
            {
                Message.Append(refEIN + "" + propertyName + " " + ex.Message + "<br>");
                value = null;
                log.Error("Error in Illegal. " + propertyName + " " + ex.Message);
            }
            return value;
        }


        public string SetISOFormat(string date, string propertyName, string ein = null)
        {
            string refEIN = string.Empty;
            try
            {   //return Convert.ToDateTime(date).ToString("yyyy-MM-dd");
                refEIN = string.IsNullOrEmpty(ein) ? string.Empty : "Ref EIN." + ein + " ";
                if (string.IsNullOrEmpty(date))
                {
                    date = "";
                }
                //else if (!string.IsNullOrEmpty(date) && !regxDateRSBY.IsMatch(date))
                //{
                //    Message.Append(refEIN + "" + "Invalid Date.Date format must be (MM/dd/yyyy) " + propertyName + " column.Check RowNo: " + RowNo + "<br>");
                //    date = null;
                //}
                else if (!string.IsNullOrEmpty(date))
                {
                    date =  Convert.ToDateTime(date).ToString("yyyy-MM-dd");                    
                }
            }
           
            catch (Exception ex)
            {
                log.Error("Error in SetISOFormat." + ex.Message);
                throw new Exception("RowNo." + RowNo + " Invalid Date. ");
            }
            return date;
        }
    }
}
