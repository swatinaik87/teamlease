﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using TeamLeaseApp;

namespace TeamLeaseApp
{
    public class MailToPerson
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //public static void MailTo(E_EXCEL_INFO objInsuredPerson, string AdminName)
        //{
        //    try
        //    {
        //        log.Info("Mail to person requested.");
        //        if (objInsuredPerson != null)
        //        {
        //            int SINo = 2;
        //            log.Info("Mail to person object status:ok");
        //            E_EXCEL_INFO InsuredPerson = objInsuredPerson.InsuredPerson;
        //            List<E_DEPENDENTS> DependentsSelf = objInsuredPerson.DependentsSelf;
        //            List<E_DEPENDENTS> DependentsParents = objInsuredPerson.DependentsParents;

        //            List<KeyValuePair<string, string>> replacementValues = new List<KeyValuePair<string, string>>();
        //            replacementValues.Add(new KeyValuePair<string, string>("MTNAME", InsuredPerson.INSUREDNAME));
        //            replacementValues.Add(new KeyValuePair<string, string>("MTEMPNO", InsuredPerson.EIN));
        //            replacementValues.Add(new KeyValuePair<string, string>("MTDOB", InsuredPerson.DOB));
        //            replacementValues.Add(new KeyValuePair<string, string>("MTCOMPANY", InsuredPerson.UNDERWRITERNAME));
        //            replacementValues.Add(new KeyValuePair<string, string>("MTDOE", InsuredPerson.ENTEREDON));
        //            replacementValues.Add(new KeyValuePair<string, string>("MTMOB", InsuredPerson.EMPMOBILE));
        //            replacementValues.Add(new KeyValuePair<string, string>("MTEML", InsuredPerson.EMPEMAIL));

        //            if (!string.IsNullOrEmpty(AdminName))
        //                replacementValues.Add(new KeyValuePair<string, string>("ADMNEDT", AdminName));

        //            if (DependentsSelf != null && DependentsSelf.Count != 0)
        //            {
        //                int isDep1 = 0;
        //                for (int i = 0; i < DependentsSelf.Count; i++)
        //                {
        //                    if (DependentsSelf[i].DEP_RELATIONCODE == 1 || DependentsSelf[i].DEP_RELATIONCODE == 7)
        //                    {
        //                        replacementValues.Add(new KeyValuePair<string, string>("MTSSNO", SINo.ToString() + "."));
        //                        replacementValues.Add(new KeyValuePair<string, string>("MTSNAME", DependentsSelf[i].DEP_NAME));
        //                        replacementValues.Add(new KeyValuePair<string, string>("MTSDOB", SetDateFormat(DependentsSelf[i].DEP_DOB)));
        //                        replacementValues.Add(new KeyValuePair<string, string>("MTSRELATION", Relation(DependentsSelf[i].DEP_RELATIONCODE)));
        //                        SINo += 1;
        //                    }
        //                    else if (DependentsSelf[i].DEP_RELATIONCODE == 2 || DependentsSelf[i].DEP_RELATIONCODE == 3)
        //                    {
        //                        if (isDep1 == 0)
        //                        {
        //                            isDep1 += 1;
        //                            replacementValues.Add(new KeyValuePair<string, string>("MTCASNO", SINo.ToString() + "."));
        //                            replacementValues.Add(new KeyValuePair<string, string>("MTCANAME", DependentsSelf[i].DEP_NAME));
        //                            replacementValues.Add(new KeyValuePair<string, string>("MTCADOB", SetDateFormat(DependentsSelf[i].DEP_DOB)));
        //                            replacementValues.Add(new KeyValuePair<string, string>("MTCARELATION", Relation(DependentsSelf[i].DEP_RELATIONCODE)));
        //                            SINo += 1;
        //                        }
        //                        else
        //                        {
        //                            replacementValues.Add(new KeyValuePair<string, string>("MTCBSNO", SINo.ToString() + "."));
        //                            replacementValues.Add(new KeyValuePair<string, string>("MTCBNAME", DependentsSelf[i].DEP_NAME));
        //                            replacementValues.Add(new KeyValuePair<string, string>("MTCBDOB", SetDateFormat(DependentsSelf[i].DEP_DOB)));
        //                            replacementValues.Add(new KeyValuePair<string, string>("MTCBRELATION", Relation(DependentsSelf[i].DEP_RELATIONCODE)));
        //                            SINo += 1;
        //                        }
        //                    }
        //                }
        //            }

        //            if (DependentsParents != null && DependentsParents.Count != 0)
        //            {
        //                int isDep1 = 0;
        //                for (int i = 0; i < DependentsParents.Count; i++)
        //                {
        //                    if (isDep1 == 0 && i == 0)
        //                    {
        //                        isDep1 += 1;
        //                        replacementValues.Add(new KeyValuePair<string, string>("MTPASNO", SINo.ToString() + "."));
        //                        replacementValues.Add(new KeyValuePair<string, string>("MTPANAME", DependentsParents[i].DEP_NAME));
        //                        replacementValues.Add(new KeyValuePair<string, string>("MTPADOB", SetDateFormat(DependentsParents[i].DEP_DOB)));
        //                        replacementValues.Add(new KeyValuePair<string, string>("MTPARELATION", Relation(DependentsParents[i].DEP_RELATIONCODE)));
        //                        SINo += 1;
        //                    }
        //                    else
        //                    {
        //                        replacementValues.Add(new KeyValuePair<string, string>("MTPBSNO", SINo.ToString() + "."));
        //                        replacementValues.Add(new KeyValuePair<string, string>("MTPBNAME", DependentsParents[i].DEP_NAME));
        //                        replacementValues.Add(new KeyValuePair<string, string>("MTPBDOB", SetDateFormat(DependentsParents[i].DEP_DOB)));
        //                        replacementValues.Add(new KeyValuePair<string, string>("MTPBRELATION", Relation(DependentsParents[i].DEP_RELATIONCODE)));
        //                        SINo += 1;
        //                    }
        //                }
        //            }

        //            int depCount = DependentsParents != null ? DependentsParents.Count : 0;
        //            replacementValues.Add(new KeyValuePair<string, string>("MTBSI", IndianCurrency(InsuredPerson.SUMINSURED)));//BASE SI
        //            replacementValues.Add(new KeyValuePair<string, string>("MTASI", IndianCurrency(depCount == 2 ? (InsuredPerson.ADDONSI / 2) : InsuredPerson.ADDONSI)));//ADD ON SI
        //            replacementValues.Add(new KeyValuePair<string, string>("MTTSI", IndianCurrency(InsuredPerson.TOPUPSI)));//TOP UP SI
        //            replacementValues.Add(new KeyValuePair<string, string>("MTPP", IndianCurrency(InsuredPerson.ADDONPREMIUM)));//TOP UP PREMIUM
        //            replacementValues.Add(new KeyValuePair<string, string>("MTTP", IndianCurrency(InsuredPerson.TOPUPPREMIUM)));
        //            replacementValues.Add(new KeyValuePair<string, string>("MTTTP", IndianCurrency(InsuredPerson.TOPUPPREMIUM + InsuredPerson.ADDONPREMIUM)));



        //            string apiUrl = "https://mandrillapp.com/api/1.0/messages/send-template.json";

        //            string mandrillKey = System.Configuration.ConfigurationManager.AppSettings["mandrillkey"].ToString();
        //            // string mandrillKey = "2hbRuHb8VAjbxe2sjatd-w";
        //            if (!string.IsNullOrEmpty(mandrillKey))
        //            {
        //                string mandrillTemplateId = "enroll_vf_confirm";

        //                List<string> toEmails = new List<string>();
        //                //toEmails.Add("kiran.evls@gmail.com");
        //                // toEmails.Add("vijay.m@affineit.com");
        //                //toEmails.Add("anand.kumar@isharemail.in");
        //                toEmails.Add(InsuredPerson.EMPEMAIL.Trim());

        //                //list of to names for emails above
        //                List<string> toNames = new List<string>();
        //                //toNames.Add("vijay.m@affineit.com");
        //                //toNames.Add("venroll@ghpltpa.com");
        //                toNames.Add("venrol@ghplmail.in");
        //                //toEmails.Add("anand.kumar@isharemail.in");

        //                dynamic sendParams = new System.Dynamic.ExpandoObject();
        //                sendParams.key = mandrillKey;
        //                sendParams.template_name = mandrillTemplateId;
        //                sendParams.template_content = new List<dynamic>();

        //                sendParams.message = new System.Dynamic.ExpandoObject();
        //                sendParams.message.to = new List<dynamic>();

        //                //to emails
        //                for (int x = 0; x < toEmails.Count; x++)
        //                {
        //                    sendParams.message.to.Add(new System.Dynamic.ExpandoObject());
        //                    sendParams.message.to[x].email = toEmails[x];
        //                }

        //                //to names - in same order as to emails    
        //                for (int x = 0; x < toNames.Count; x++)
        //                {
        //                    //dont add if email wasnt added
        //                    if (toEmails.Count >= x + 1)
        //                    {
        //                        sendParams.message.to[x].name = toNames[x];
        //                    }
        //                }

        //                //additional mandrill options
        //                sendParams.message.track_opens = true;
        //                //sendParams.message.track_clicks = true;

        //                //create replacement values object to merge in
        //                sendParams.message.global_merge_vars = new List<dynamic>();

        //                int counter = 0;
        //                foreach (var pair in replacementValues)
        //                {
        //                    sendParams.message.global_merge_vars.Add(new System.Dynamic.ExpandoObject());
        //                    sendParams.message.global_merge_vars[counter].name = pair.Key;
        //                    sendParams.message.global_merge_vars[counter].content = pair.Value;

        //                    counter++;
        //                }


        //                //json send parameters
        //                string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(sendParams);


        //                var http = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(new Uri(apiUrl));
        //                http.Accept = "application/json";
        //                http.ContentType = "application/json";
        //                http.Method = "POST";

        //                System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
        //                Byte[] bytes = encoding.GetBytes(jsonString);

        //                System.IO.Stream newStream = http.GetRequestStream();
        //                newStream.Write(bytes, 0, bytes.Length);
        //                newStream.Close();

        //                var response = http.GetResponse();

        //                var stream = response.GetResponseStream();
        //                var sr = new System.IO.StreamReader(stream);
        //                var content = sr.ReadToEnd();

        //                if (content != null)
        //                {
        //                    if (!string.IsNullOrWhiteSpace(content))
        //                    {
        //                        dynamic resultObj = Newtonsoft.Json.JsonConvert.DeserializeObject(content);
        //                        if (resultObj != null)
        //                        {
        //                            if (resultObj[0] != null)
        //                            {
        //                                if (resultObj[0].status == "sent")
        //                                {
        //                                    log.Info("mail status:success");
        //                                    log.Info("TO:" + resultObj[0].email);
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        log.Info("mail status:fail");
        //                        log.Info("inavlid response from mail.");
        //                    }
        //                }

        //                //create web client to post data
        //                //var request = new System.Net.WebClient();

        //                //request.Headers.Add("Content-Type", "application/json");

        //                //set content type to json since we are posting json data
        //                //request.Headers[HttpRequestHeader.ContentType] = "application/json";

        //                //log.Info("Mail sending initialised....");
        //                //post json data and get the response
        //                //string responseString = request.UploadString(apiUrl, "POST", jsonString);
        //                //log.Info("got response from mail...");


        //                //if (!string.IsNullOrWhiteSpace(responseString))
        //                //{
        //                //    dynamic resultObj = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);
        //                //    if (resultObj != null)
        //                //    {
        //                //        if (resultObj[0] != null)
        //                //        {
        //                //            if (resultObj[0].status == "sent")
        //                //            {
        //                //                log.Info("mail status:success");
        //                //                log.Info("TO:" + resultObj[0].email);
        //                //            }
        //                //        }
        //                //    }
        //                //}
        //                //else
        //                //{
        //                //    log.Info("mail status:fail");
        //                //    log.Info("inavlid response from mail.");
        //                //}
        //            }
        //            else
        //            {
        //                log.Info("mandrill key not found. please check web.config");
        //                log.Error("mandrill key not found. please check web.config");
        //            }
        //        }
        //        else
        //            log.Info("Mail to person object status:fail");
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error in MailToPerson.vodafone cnf" + ex.Message);
        //    }
        //}

        //public static void MailToFAI(E_EXCEL_INFO objInsuredPerson, string AdminName)
        //{
        //    try
        //    {
        //        log.Info("Mail to person(FAI) requested.");
        //        if (objInsuredPerson != null)
        //        {
        //            int SINo = 2;
        //            log.Info("Mail to person object status:ok");
        //            E_EXCEL_INFO InsuredPerson = objInsuredPerson.InsuredPerson;
        //            List<E_DEPENDENTS> DependentsSelf = objInsuredPerson.DependentsSelf;
        //            List<E_DEPENDENTS> DependentsParents = objInsuredPerson.DependentsParents;

        //            List<KeyValuePair<string, string>> replacementValues = new List<KeyValuePair<string, string>>();
        //            replacementValues.Add(new KeyValuePair<string, string>("MTNAME", InsuredPerson.INSUREDNAME));
        //            replacementValues.Add(new KeyValuePair<string, string>("MTEMPNO", InsuredPerson.EIN));
        //            replacementValues.Add(new KeyValuePair<string, string>("MTDOB", InsuredPerson.DOB));
        //            replacementValues.Add(new KeyValuePair<string, string>("MTCOMPANY", InsuredPerson.UNDERWRITERNAME));
        //            replacementValues.Add(new KeyValuePair<string, string>("MTDOE", InsuredPerson.ENTEREDON));
        //            replacementValues.Add(new KeyValuePair<string, string>("MTMOB", InsuredPerson.EMPMOBILE));
        //            replacementValues.Add(new KeyValuePair<string, string>("MTEML", InsuredPerson.EMPEMAIL));

        //            if (!string.IsNullOrEmpty(AdminName))
        //                replacementValues.Add(new KeyValuePair<string, string>("ADMNEDT", AdminName));

        //            if (DependentsSelf != null && DependentsSelf.Count != 0)
        //            {
        //                int isDep1 = 0;
        //                for (int i = 0; i < DependentsSelf.Count; i++)
        //                {
        //                    if (DependentsSelf[i].DEP_RELATIONCODE == 1 || DependentsSelf[i].DEP_RELATIONCODE == 7)
        //                    {
        //                        replacementValues.Add(new KeyValuePair<string, string>("MTSSNO", SINo.ToString() + "."));
        //                        replacementValues.Add(new KeyValuePair<string, string>("MTSNAME", DependentsSelf[i].DEP_NAME));
        //                        replacementValues.Add(new KeyValuePair<string, string>("MTSDOB", SetDateFormat(DependentsSelf[i].DEP_DOB)));
        //                        replacementValues.Add(new KeyValuePair<string, string>("MTSRELATION", Relation(DependentsSelf[i].DEP_RELATIONCODE)));
        //                        SINo += 1;
        //                    }
        //                    else if (DependentsSelf[i].DEP_RELATIONCODE == 2 || DependentsSelf[i].DEP_RELATIONCODE == 3)
        //                    {
        //                        if (isDep1 == 0)
        //                        {
        //                            isDep1 += 1;
        //                            replacementValues.Add(new KeyValuePair<string, string>("MTCASNO", SINo.ToString() + "."));
        //                            replacementValues.Add(new KeyValuePair<string, string>("MTCANAME", DependentsSelf[i].DEP_NAME));
        //                            replacementValues.Add(new KeyValuePair<string, string>("MTCADOB", SetDateFormat(DependentsSelf[i].DEP_DOB)));
        //                            replacementValues.Add(new KeyValuePair<string, string>("MTCARELATION", Relation(DependentsSelf[i].DEP_RELATIONCODE)));
        //                            SINo += 1;
        //                        }
        //                        else
        //                        {
        //                            replacementValues.Add(new KeyValuePair<string, string>("MTCBSNO", SINo.ToString() + "."));
        //                            replacementValues.Add(new KeyValuePair<string, string>("MTCBNAME", DependentsSelf[i].DEP_NAME));
        //                            replacementValues.Add(new KeyValuePair<string, string>("MTCBDOB", SetDateFormat(DependentsSelf[i].DEP_DOB)));
        //                            replacementValues.Add(new KeyValuePair<string, string>("MTCBRELATION", Relation(DependentsSelf[i].DEP_RELATIONCODE)));
        //                            SINo += 1;
        //                        }
        //                    }
        //                }
        //            }

        //            if (DependentsParents != null && DependentsParents.Count != 0)
        //            {
        //                int isDep1 = 0;
        //                for (int i = 0; i < DependentsParents.Count; i++)
        //                {
        //                    if (isDep1 == 0 && i == 0)
        //                    {
        //                        isDep1 += 1;
        //                        replacementValues.Add(new KeyValuePair<string, string>("MTPASNO", SINo.ToString() + "."));
        //                        replacementValues.Add(new KeyValuePair<string, string>("MTPANAME", DependentsParents[i].DEP_NAME));
        //                        replacementValues.Add(new KeyValuePair<string, string>("MTPADOB", SetDateFormat(DependentsParents[i].DEP_DOB)));
        //                        replacementValues.Add(new KeyValuePair<string, string>("MTPARELATION", Relation(DependentsParents[i].DEP_RELATIONCODE)));
        //                        SINo += 1;
        //                    }
        //                    else
        //                    {
        //                        replacementValues.Add(new KeyValuePair<string, string>("MTPBSNO", SINo.ToString() + "."));
        //                        replacementValues.Add(new KeyValuePair<string, string>("MTPBNAME", DependentsParents[i].DEP_NAME));
        //                        replacementValues.Add(new KeyValuePair<string, string>("MTPBDOB", SetDateFormat(DependentsParents[i].DEP_DOB)));
        //                        replacementValues.Add(new KeyValuePair<string, string>("MTPBRELATION", Relation(DependentsParents[i].DEP_RELATIONCODE)));
        //                        SINo += 1;
        //                    }
        //                }
        //            }

        //            int depCount = DependentsParents != null ? DependentsParents.Count : 0;
        //            replacementValues.Add(new KeyValuePair<string, string>("MTBSI", IndianCurrency(InsuredPerson.SUMINSURED)));//BASE SI
        //            replacementValues.Add(new KeyValuePair<string, string>("MTASI", IndianCurrency(depCount == 2 ? (InsuredPerson.ADDONSI / 2) : InsuredPerson.ADDONSI)));//ADD ON SI
        //            replacementValues.Add(new KeyValuePair<string, string>("MTTSI", IndianCurrency(InsuredPerson.TOPUPSI)));//TOP UP SI
        //            replacementValues.Add(new KeyValuePair<string, string>("MTPP", IndianCurrency(InsuredPerson.ADDONPREMIUM)));//TOP UP PREMIUM
        //            replacementValues.Add(new KeyValuePair<string, string>("MTTP", IndianCurrency(InsuredPerson.TOPUPPREMIUM)));
        //            replacementValues.Add(new KeyValuePair<string, string>("MTTTP", IndianCurrency(InsuredPerson.TOPUPPREMIUM + InsuredPerson.ADDONPREMIUM)));



        //            string apiUrl = "https://mandrillapp.com/api/1.0/messages/send-template.json";

        //            string mandrillKey = System.Configuration.ConfigurationManager.AppSettings["mandrillkey"].ToString();
        //            // string mandrillKey = "2hbRuHb8VAjbxe2sjatd-w";
        //            if (!string.IsNullOrEmpty(mandrillKey))
        //            {
        //                //string mandrillTemplateId = "enroll_vf_confirm";
        //                string mandrillTemplateId = "ienroll-plain";

        //                List<string> toEmails = new List<string>();
        //                //toEmails.Add("surya.kiran@isbsindia.in");
        //                // toEmails.Add("vijay.m@affineit.com");
        //                //toEmails.Add("anand.kumar@isharemail.in");
        //                toEmails.Add(InsuredPerson.EMPEMAIL.Trim());

        //                //list of to names for emails above
        //                List<string> toNames = new List<string>();
        //                //toNames.Add("vijay.m@affineit.com");
        //                //toNames.Add("venroll@ghpltpa.com");
        //                //toNames.Add("venrol@ghplmail.in");
        //                toNames.Add("ienroll@ghpltpa.com");
        //                //toEmails.Add("anand.kumar@isharemail.in");

        //                dynamic sendParams = new System.Dynamic.ExpandoObject();
        //                sendParams.key = mandrillKey;
        //                sendParams.template_name = mandrillTemplateId;
        //                sendParams.template_content = new List<dynamic>();

        //                sendParams.message = new System.Dynamic.ExpandoObject();
        //                sendParams.message.to = new List<dynamic>();

        //                //to emails
        //                for (int x = 0; x < toEmails.Count; x++)
        //                {
        //                    sendParams.message.to.Add(new System.Dynamic.ExpandoObject());
        //                    sendParams.message.to[x].email = toEmails[x];
        //                }

        //                //to names - in same order as to emails    
        //                for (int x = 0; x < toNames.Count; x++)
        //                {
        //                    //dont add if email wasnt added
        //                    if (toEmails.Count >= x + 1)
        //                    {
        //                        sendParams.message.to[x].name = toNames[x];
        //                    }
        //                }

        //                //additional mandrill options
        //                sendParams.message.track_opens = true;
        //                //sendParams.message.track_clicks = true;

        //                //create replacement values object to merge in
        //                sendParams.message.global_merge_vars = new List<dynamic>();

        //                int counter = 0;
        //                foreach (var pair in replacementValues)
        //                {
        //                    sendParams.message.global_merge_vars.Add(new System.Dynamic.ExpandoObject());
        //                    sendParams.message.global_merge_vars[counter].name = pair.Key;
        //                    sendParams.message.global_merge_vars[counter].content = pair.Value;

        //                    counter++;
        //                }


        //                //json send parameters
        //                string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(sendParams);

        //                //create web client to post data
        //                var request = new System.Net.WebClient();

        //                request.Headers.Add("Content-Type", "application/json");

        //                //set content type to json since we are posting json data
        //                //request.Headers[HttpRequestHeader.ContentType] = "application/json";

        //                log.Info("Mail sending initialised....");
        //                //post json data and get the response
        //                string responseString = request.UploadString(apiUrl, "POST", jsonString);
        //                log.Info("got response from mail...");


        //                if (!string.IsNullOrWhiteSpace(responseString))
        //                {
        //                    dynamic resultObj = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);
        //                    if (resultObj != null)
        //                    {
        //                        if (resultObj[0] != null)
        //                        {
        //                            if (resultObj[0].status == "sent")
        //                            {
        //                                log.Info("mail status:success");
        //                                log.Info("TO:" + resultObj[0].email);
        //                            }
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    log.Info("mail status:fail");
        //                    log.Info("inavlid response from mail.");
        //                }
        //            }
        //            else
        //            {
        //                log.Info("mandrill key not found. please check web.config");
        //                log.Error("mandrill key not found. please check web.config");
        //            }
        //        }
        //        else
        //            log.Info("Mail to person object status:fail");
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error in MailToPerson. " + ex.Message);
        //    }
        //}

    

        //private static string SetDateFormat(string date)
        //{
        //    string returndate = string.Empty;
        //    try
        //    {
        //        returndate = Convert.ToDateTime(date).ToString("dd-MM-yyyy");
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error in SetDateFormat. " + ex.Message);
        //    }
        //    return returndate;
        //}

        //public static string WelComeReminderMail(E_MAILS objMail, int welRem, string testEmail, int compId)
        //{
        //    string status = string.Empty;
        //    try
        //    {
        //        string templateName = DBAccess.GetTemplateName(compId);
        //        if (!string.IsNullOrEmpty(templateName))
        //        {
        //            log.Info("Mail to person(FAI welcome or remider) requested.");
        //            if (objMail != null)
        //            {
        //                status = "Mail sending requested...EMPID:" + objMail.EIN + "<br/>";
        //                log.Info("Mail to person object status(FAI welcome or remider):ok");

        //                List<KeyValuePair<string, string>> replacementValues = new List<KeyValuePair<string, string>>();
        //                replacementValues.Add(new KeyValuePair<string, string>("MTFROMDT", objMail.ENROLLFROM));
        //                replacementValues.Add(new KeyValuePair<string, string>("MTTODT", objMail.ENROLLTO));
        //                replacementValues.Add(new KeyValuePair<string, string>("MTPOLFROMDT", objMail.POLICYFROM));
        //                replacementValues.Add(new KeyValuePair<string, string>("MTPOLTODT", objMail.POLICYTO));
        //                replacementValues.Add(new KeyValuePair<string, string>("MTEMPNO", objMail.EIN));
        //                replacementValues.Add(new KeyValuePair<string, string>("MTNAME", objMail.INSUREDNAME));
        //                replacementValues.Add(new KeyValuePair<string, string>("MTCOMPANY", objMail.COMPANYNAME));
        //                replacementValues.Add(new KeyValuePair<string, string>("MTURL", objMail.DIR));
        //                replacementValues.Add(new KeyValuePair<string, string>("MTLOGIN", objMail.EIN));
        //                replacementValues.Add(new KeyValuePair<string, string>("MTPWD", objMail.PWORD));
        //                replacementValues.Add(new KeyValuePair<string, string>("MTCTCTEMAIL", objMail.CEMAIL));
        //                replacementValues.Add(new KeyValuePair<string, string>("MTCTCTPHONE", objMail.CMOBILE));
        //                replacementValues.Add(new KeyValuePair<string, string>("MTCHKRENREM", welRem.ToString()));

        //                string apiUrl = "https://mandrillapp.com/api/1.0/messages/send-template.json";

        //                string mandrillKey = System.Configuration.ConfigurationManager.AppSettings["mandrillkey"].ToString();
        //                // string mandrillKey = "2hbRuHb8VAjbxe2sjatd-w";
        //                if (!string.IsNullOrEmpty(mandrillKey))
        //                {
        //                    //string mandrillTemplateId = "enroll_vf_confirm";
        //                    //string mandrillTemplateId = "enroll_FAI_invite";//
        //                    string mandrillTemplateId = templateName;

        //                    List<string> toEmails = new List<string>();
        //                    if (string.IsNullOrEmpty(testEmail))
        //                        toEmails.Add(objMail.EMPEMAIL.Trim());
        //                    else
        //                        toEmails.Add(testEmail);
        //                    //toEmails.Add("surya.kiran@isbsindia.in");
        //                    //toEmails.Add("vijay.m@affineit.com");
        //                    //toEmails.Add("anand.kumar@isharemail.in");
        //                    // toEmails.Add(objMail.EMPEMAIL.Trim());

        //                    //list of to names for emails above
        //                    List<string> toNames = new List<string>();
        //                    //toNames.Add("vijay.m@affineit.com");
        //                    //toNames.Add("venroll@ghpltpa.com");
        //                    //toNames.Add("venrol@ghplmail.in");
        //                    toNames.Add("ienroll@ghpltpa.com");
        //                    //toEmails.Add("anand.kumar@isharemail.in");

        //                    dynamic sendParams = new System.Dynamic.ExpandoObject();
        //                    sendParams.key = mandrillKey;
        //                    sendParams.template_name = mandrillTemplateId;
        //                    sendParams.template_content = new List<dynamic>();

        //                    sendParams.message = new System.Dynamic.ExpandoObject();
        //                    sendParams.message.to = new List<dynamic>();

        //                    //to emails
        //                    for (int x = 0; x < toEmails.Count; x++)
        //                    {
        //                        sendParams.message.to.Add(new System.Dynamic.ExpandoObject());
        //                        sendParams.message.to[x].email = toEmails[x];
        //                    }

        //                    //to names - in same order as to emails    
        //                    for (int x = 0; x < toNames.Count; x++)
        //                    {
        //                        //dont add if email wasnt added
        //                        if (toEmails.Count >= x + 1)
        //                        {
        //                            sendParams.message.to[x].name = toNames[x];
        //                        }
        //                    }

        //                    //additional mandrill options
        //                    sendParams.message.track_opens = true;
        //                    //sendParams.message.track_clicks = true;

        //                    //create replacement values object to merge in
        //                    sendParams.message.global_merge_vars = new List<dynamic>();

        //                    int counter = 0;
        //                    foreach (var pair in replacementValues)
        //                    {
        //                        sendParams.message.global_merge_vars.Add(new System.Dynamic.ExpandoObject());
        //                        sendParams.message.global_merge_vars[counter].name = pair.Key;
        //                        sendParams.message.global_merge_vars[counter].content = pair.Value;

        //                        counter++;
        //                    }


        //                    //json send parameters
        //                    string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(sendParams);

        //                    //create web client to post data
        //                    var request = new System.Net.WebClient();

        //                    request.Headers.Add("Content-Type", "application/json");

        //                    //set content type to json since we are posting json data
        //                    //request.Headers[HttpRequestHeader.ContentType] = "application/json";

        //                    status = string.Concat(status, "Mail sending initialised....<br/>");
        //                    log.Info("Mail sending initialised....");
        //                    //post json data and get the response
        //                    string responseString = request.UploadString(apiUrl, "POST", jsonString);
        //                    log.Info("got response from mail...");
        //                    status = string.Concat(status, "got response from mail...<br/>");

        //                    if (!string.IsNullOrWhiteSpace(responseString))
        //                    {
        //                        dynamic resultObj = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);
        //                        if (resultObj != null)
        //                        {
        //                            if (resultObj[0] != null)
        //                            {
        //                                status = string.Concat(status, "status:", resultObj[0].status, "<br/>");
        //                                if (resultObj[0].status == "sent")
        //                                {
        //                                    log.Info("mail status:success");
        //                                    log.Info("TO:" + resultObj[0].email);
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        log.Info("mail status:fail");
        //                        log.Info("inavlid response from mail.");
        //                    }
        //                }
        //                else
        //                {
        //                    log.Info("mandrill key not found. please check web.config");
        //                    log.Error("mandrill key not found. please check web.config");
        //                }
        //            }
        //            else
        //                log.Info("Mail to person object status:fail");
        //        }
        //        else
        //            status = "Unable to get template name.Please contact admin.<br/>";
        //    }
        //    catch (Exception ex)
        //    {
        //        status = string.Concat(status, "Error while sending.Ref EIN " + objMail.EIN);
        //        log.Error("Error in WelComeReminderMail. " + ex.Message);
        //    }
        //    return status;
        //}

        //public static string SentWelComeMail(int welRem, long lotno, int liveOrTest, string email, int compId, ref string message)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    try
        //    {
        //        sb.Append("Requested for Mails...<br/>");
        //        List<E_MAILS> objMails = DBAccess.GetEmailList(lotno, ref message);
        //        if (objMails != null && objMails.Count != 0)
        //        {
        //            if (string.IsNullOrEmpty(email))
        //            {
        //                for (int i = 0; i < objMails.Count; i++)
        //                {
        //                    string status = WelComeReminderMail(objMails[i], welRem, email, compId);
        //                    sb.Append(status);
        //                }
        //            }
        //            else
        //            {
        //                string status = WelComeReminderMail(objMails[0], welRem, email, compId);
        //                sb.Append(status);
        //            }
        //        }
        //        else
        //            sb.Append("we are unable to get emails list to send.Please contact administrator.<br/>");
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error in SentWelComeMail. " + ex.Message);
        //    }
        //    return sb.ToString();
        //}
    }
}
