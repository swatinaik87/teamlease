﻿using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using System.Configuration;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Net;

namespace TeamLeaseApp
{
   
    public class ValidateExcel
    {
        private static readonly log4net.ILog _logger =
  log4net.LogManager.GetLogger(typeof(Form1));
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public long ToExcel(string FilePath, string Extension, string compId, string enrollFrom, string enrollTo, ref string errMessage)
         {
            long lotNo = 0;
            DataTable dataTable = null;
            try
            {
                //Read Excel file and converted into datatable

                dataTable = ImportExcel(FilePath, Extension, "YES");             
              
               
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                   // List<E_DROPDWON> objBand = DBAccess.GetCompanyBand(Convert.ToInt32(compId));
                   // System.Web.HttpContext.Current.Session["band"] = objBand[0].Value;
                    log.Info("Data Table Count:" + dataTable.Rows.Count);
                    //List<E_EXCEL_INSURED_PERSON> objExcel = ConvertDataTableToList(dataTable);
                    List<E_EXCEL_INFO> objExcel = ConvertDataTableToList(FilePath, dataTable);
                    if (objExcel != null && objExcel.Count != 0)
                    {
                        List<StringBuilder> str = objExcel.Where(s => s.Message.ToString().Length > 0).Select(s => s.Message).ToList();


                        //if (str.Count == 0) //Commented by annapurna
                        if (str.Count != 0)
                        {
                            log.Info("Excel file generic list Count: " + objExcel.Count);
                            //lotNo = InsertXML(objExcel, errMessage);
                            lotNo = InsertXML(FilePath,objExcel, ref errMessage);
                            string mmm = SendMail_ErrorRecs(FilePath, str);
                            // objExcel, Convert.ToInt32(compId), enrollFrom, enrollTo, ref errMessage
                        }
                        else
                        {
                            objExcel = null;

                          string mmm=  SendMail_ErrorRecs(FilePath,str);
                            
                            //StringBuilder sb = new StringBuilder();
                            //for (int i = 0; i < str.Count; i++)
                            //    sb.Append(str[i].ToString());
                            //errMessage = sb.ToString();
                        }
                    }
                    else
                    {
                        log.Info("DataTable to generic list failed.");
                    }
                    }
               
                else
                {
                    log.Info("No rows in DataTable Excel or problem while reading.");
                }
            }
            catch (Exception ex)
            {
                lotNo = 0;
                log.Error("Error in Reading excel file. " + ex.Message);
            }
            return lotNo;
        }



        public string SendMail_ErrorRecs(string Filepath,List<StringBuilder> str)
        {
            string ms = "";
            try
            {
                string userName = System.Configuration.ConfigurationManager.AppSettings["userName"].ToString();
                string password = System.Configuration.ConfigurationManager.AppSettings["password"].ToString();
                string HostName = System.Configuration.ConfigurationManager.AppSettings["HostName"].ToString();
                string mailFrom = System.Configuration.ConfigurationManager.AppSettings["mailFrom"].ToString();
                string mailTo = System.Configuration.ConfigurationManager.AppSettings["mailTo"].ToString();
                //string mailCC = System.Configuration.ConfigurationManager.AppSettings["mailCC"].ToString();
                string FileName = System.IO.Path.GetFileName(Filepath);

                string textBody = "Hi Team,<br> We are getting below errors when we are processing the data.Please find the details and correct the data.FileName-"+ FileName + "<br><br>";
                    textBody+=" <table border=" + 1 + " cellpadding=" + 0 + " cellspacing=" + 0 + " width = " + 600 + "><tr bgcolor='#4da6ff'><td><b>SNo</b></td> <td> <b> Error Message</b> </td></tr>";
               
                for (int i = 0; i < str.Count; i++)
                {
                    textBody += "<tr><td>" + Convert.ToInt32(i + 1) + "</td><td> " + str[i] + "</td> </tr>";
                }
                textBody += "</table>";
                textBody += "<br>Thanks & Regards,<br>Rajender.";
                MailMessage mail = new MailMessage();
                System.Net.Mail.SmtpClient SmtpServer1 = new SmtpClient(HostName);

                mail.From = new MailAddress(mailFrom);                
                mail.To.Add(mailTo);
                //mail.CC.Add(mailCC);
                mail.Subject = "Errors in Input File Data";
                mail.Body = textBody;
                mail.IsBodyHtml = true;
                SmtpServer1.Port = 587;
                SmtpServer1.Credentials = new System.Net.NetworkCredential(userName, password);
                SmtpServer1.EnableSsl = true;
                

                SmtpServer1.Send(mail);
                ms = "";
            }
            catch (Exception ex)
            {
                ms = ex.Message.ToString();
                log.Error("Error in sending email. " + ex.Message);
             
                
            }
            return ms;
        }
        #region ConvertDataTableToList
        private static List<E_EXCEL_INFO> ConvertDataTableToList(string FilePath, DataTable xLDt)
        {
            //log.Info("Data Table to generic list conversion started.");
           // WriteLogFile.WriteLog(String.Format("{0} @ {1}", "Data Table to generic list conversion started.", DateTime.Now));
            List<E_EXCEL_INFO> objExcelInfo = new List<E_EXCEL_INFO>();
            string errmsg = string.Empty;
            try
            {

                int dataRowCount = xLDt.Rows.Count;
                for (int i = 0; i < dataRowCount; i++)
                {
                    DataRow row = xLDt.Rows[i];
                    //added by annapurna
                    DateTime dtDOB = DateTime.MinValue;
                    DateTime dtDOJ = DateTime.MinValue;
                    dtDOB = Convert.ToDateTime(row["DOB"].ToString());
                    dtDOJ = Convert.ToDateTime(row["DOJ"].ToString());
                    //end by annapurna
                    objExcelInfo.Add(new E_EXCEL_INFO()
                    {
                        RowNo = i + 2,
                                   CLIENT_ID = row["CLIENT_ID"] == DBNull.Value ? null : row["CLIENT_ID"].ToString().Trim(),
                        CLIENT_NAME = row["CLIENT_NAME"] == DBNull.Value ? null : row["CLIENT_NAME"].ToString().Trim(),
                        EMPLOYEE_NO = row["EMPLOYEE_NO"] == DBNull.Value ? null : row["EMPLOYEE_NO"].ToString().Trim(),
                            ASSOCIATE_NAME = row["ASSOCIATE_NAME"] == DBNull.Value ? null : row["ASSOCIATE_NAME"].ToString().Trim(),
                        GENDER = row["GENDER"] == DBNull.Value ? null : row["GENDER"].ToString(),
                       // DOB = row["DOB"] == DBNull.Value ? null :row["DOB"].ToString().Trim(),//Commented by Annapurna
                        DOB = row["DOB"] == DBNull.Value ? null : dtDOB.ToString("MM/dd/yyyy").Trim(), //Added by Annapurna (in server date is saving as dd-mm-yyyy format in Excel sheet C:\Download, so converting to mm/dd/yyyy)
                        DESIGNATION = row["DESIGNATION"] == DBNull.Value ? null : row["DESIGNATION"].ToString().Trim(),
                        INSURANCE_COMPANY_NAME = row["ASSOCIATE_NAME"] == DBNull.Value ? null : row["ASSOCIATE_NAME"].ToString().Trim(),
                        INSURANCE_POLICY_TYPE = row["INSURANCE_POLICY_TYPE"] == DBNull.Value ? null : row["INSURANCE_POLICY_TYPE"].ToString().Trim(),
                        INSURANCE_POLICY = row["INSURANCE_POLICY_TYPE"] == DBNull.Value ? null : row["INSURANCE_POLICY_TYPE"].ToString().Trim(),
                        SUM_INSURED = row["SUM_INSURED"] == DBNull.Value ? null : row["SUM_INSURED"].ToString().Trim(),
                        SUM_INSURED1 = row["SUM_INSURED1"] == DBNull.Value ? null : row["SUM_INSURED1"].ToString().Trim(),
                        // DOJ = row["DOJ"] == DBNull.Value ? null :  row["DOJ"].ToString().Trim(),//Commented by Annapurna
                       DOJ = row["DOJ"] == DBNull.Value ? null : dtDOJ.ToString("MM/dd/yyyy").Trim(), //Added by Annapurna
                        ACTUAL_DOL = row["DOB"] == DBNull.Value ? null : row["DOB"].ToString().Trim(),
                        ACTIVE_STATUS = row["ACTIVE_STATUS"] == DBNull.Value ? null : row["ACTIVE_STATUS"].ToString().Trim(),
                        STOPPAY_STATUS = row["STOPPAY_STATUS"] == DBNull.Value ? null : row["STOPPAY_STATUS"].ToString().Trim(),
                        OL_BASIC = row["SUM_INSURED"] == DBNull.Value ? "0" : row["SUM_INSURED"].ToString().Trim(),
                        OL_DA = row["SUM_INSURED"] == DBNull.Value ? "0" : row["SUM_INSURED"].ToString().Trim(),
                        OL_EMPF = row["SUM_INSURED"] == DBNull.Value ? "0" : row["SUM_INSURED"].ToString().Trim(),
                        OL_EMPCOMP = row["SUM_INSURED"] == DBNull.Value ? "0" : row["SUM_INSURED"].ToString().Trim(),
                        OL_ESIC = row["SUM_INSURED"] == DBNull.Value ? "0" : row["SUM_INSURED"].ToString().Trim(),
                        OL_CTC = row["SUM_INSURED"] == DBNull.Value ? "0" : row["SUM_INSURED"].ToString().Trim(),
                        OL_GROSS = row["SUM_INSURED"] == DBNull.Value ? "0" : row["SUM_INSURED"].ToString().Trim(),
                        SR_BASIC = row["SUM_INSURED"] == DBNull.Value ? "0" : row["SUM_INSURED"].ToString().Trim(),
                        SR_DA = row["SUM_INSURED"] == DBNull.Value ? "0" : row["SUM_INSURED"].ToString().Trim(),
                        SR_EMPF = row["SUM_INSURED"] == DBNull.Value ? "0" : row["SUM_INSURED"].ToString().Trim(),
                        SR_EMPCOMP = row["SUM_INSURED"] == DBNull.Value ? "0" : row["SUM_INSURED"].ToString().Trim(),
                        SR_ESIC = row["SUM_INSURED"] == DBNull.Value ? "0" : row["SUM_INSURED"].ToString().Trim(),
                        SR_CTC = row["SUM_INSURED"] == DBNull.Value ? "0" : row["SUM_INSURED"].ToString().Trim(),
                        SR_GROSS = row["SR_GROSS"] == DBNull.Value ? "0" : row["SR_GROSS"].ToString().Trim(),
                        SR_EFFECTIVE_DATE = row["DOB"] == DBNull.Value ? null : row["DOB"].ToString().Trim(),

                    });
                }
                // log.Info("Data Table to generic list conversion finished.");
               // WriteLogFile.WriteLog(String.Format("{0} @ {1}", "Data Table to generic list conversion finished.", DateTime.Now));
            }
            catch (Exception ex)
            {
                objExcelInfo = null;
                errmsg = ex.Message;

                _logger.Error("Error Occured while converting DataTable to Generic List.Error is" + ex.Message);
                // WriteLogFile.WriteLog(String.Format("{0} @ {1}", "Error Occured while converting DataTable to Generic List.Error is" + ex.Message, DateTime.Now));
            }
            return objExcelInfo;
        }
        #endregion

        //private static List<E_EXCEL_INFO> ConvertDataTableToList(string FilePath, DataTable xLDt)
        //{
        //    log.Info("Data Table to generic list conversion started.");
        //    List<E_EXCEL_INFO_New> objExcelInfo = new List<E_EXCEL_INFO_New>();
        //    try
        //    {
        //        int dataRowCount = xLDt.Rows.Count;          
        //        for (int i = 0; i < dataRowCount; i++)
        //        {
        //            DataRow row = xLDt.Rows[i];

        //            objExcelInfo.Add(new E_EXCEL_INFO_New()
        //            {
        //                RowNo = i + 2,

        //              //eXCEL SHEET COLUMN NAMES-  S No    Employee No Associate Name  Relation    Gender  DOB Client Id   Client Name DOJ Policy Type GMC Sum Insu    GPA sum Insu    Policy DOJ  Remarks Gross


        //                EMPLOYEE_NO = row["Employee No"] == DBNull.Value ? null : row["Employee No"].ToString().Trim(),
        //                ASSOCIATE_NAME = row["Associate Name"] == DBNull.Value ? null : row["Associate Name"].ToString().Trim(),
        //                RELATION =row["Relation"] == DBNull.Value ? null : row["Relation"].ToString().Trim(),
        //                GENDER = row["Gender"] == DBNull.Value ? null : row["Gender"].ToString(),
        //                DOB= row["DOB"] == DBNull.Value ? null : row["DOB"].ToString().Trim(),
        //                CLIENT_ID = row["Client Id"] == DBNull.Value ? null : row["Client Id"].ToString().Trim(),
        //                CLIENT_NAME = row["Client Name"] == DBNull.Value ? null : row["Client Name"].ToString().Trim(),
        //                DOJ = row["DOJ"] == DBNull.Value ? null : row["DOJ"].ToString().Trim(),
        //                POLICYTYPE = row["Policy Type"] == DBNull.Value ? null : row["Policy Type"].ToString().Trim(),
        //                GMC_SUM_INSURED = row["GMC Sum Insu"] == DBNull.Value ? null : row["GMC Sum Insu"].ToString().Trim(),
        //                GPA_SUM_INSURED = row["GPA sum Insu"] == DBNull.Value ? null : row["GPA sum Insu"].ToString().Trim(),
        //                POLICY_DOJ = row["Policy DOJ"] == DBNull.Value ? null : row["Policy DOJ"].ToString().Trim(),
        //                REMARKS = row["Remarks"] == DBNull.Value ? null : row["Remarks"].ToString().Trim(),
        //                GROSS = row["Gross"] == DBNull.Value ? null : row["Gross"].ToString().Trim(),





        //            });
        //        }
        //        log.Info("Data Table to generic list conversion finished.");
        //    }
        //    catch (Exception ex)
        //    {
        //        objExcelInfo = null;
        //        log.Error("Error Occured while converting DataTable to Generic List.Error is" + ex.Message);
        //    }

        //    return objExcelInfo;
        //}

        private DataTable ImportExcel(string FilePath, string Extension, string isHDR)
        {
            string exmsg = "";
            log.Info("Excel file reading started.");
            string conStr = string.Empty;
            OleDbConnection connExcel = null;
            OleDbCommand cmdExcel = null;
            OleDbDataAdapter oda = null;
            DataTable dataTable = null;
            try
            {
                switch (Extension)
                {
                    case ".xls": //Excel 97-03
                        conStr = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                        break;
                    case ".xlsx": //Excel 07
                        conStr = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                        break;
                }
                conStr = String.Format(conStr, FilePath, isHDR);
                log.Info("Excel file path: " + conStr);
                connExcel = new OleDbConnection(conStr);
                cmdExcel = new OleDbCommand();
                oda = new OleDbDataAdapter();
                dataTable = new DataTable();
                cmdExcel.Connection = connExcel;

                //Get the name of First Sheet
                if (connExcel.State == ConnectionState.Open)
                    connExcel.Close();
                connExcel.Open();
                DataTable dtExcelSchema;
                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                connExcel.Close();


                //Read Data from First Sheet
                connExcel.Open();
                cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";

                //cmdExcel.CommandText = "SELECT  [Emp_Code],[Employee Name],[Gender],[DOB],[Client ID],[Client Name],[D#O#J],[Policy DOJ],[Policy Type],[GMC Sum insured],[GPA sum insured],[Addition ( month ) Remarks],[DOL],[Policy DOL],[Gross Salary],[Deletion  Month ( Remarks)] From [" + SheetName + "]";
                //cmdExcel.CommandText = "SELECT  * From [" + SheetName + "]";
                //cmdExcel.CommandText = "SELECT[Client Id],[Client Name],[Employee No],[Associate Name],[Gender],[DOB],[Designation],[Insurance Company Name],[InsurancePolicyType],[InsurancePolicy],[SumInsured],[SumInsured1],[DOJ],[Actual DOL],[Active Status],[Stoppay status],[OL_Basic],[OL_DA],[OL_EMPF],[OL_EMPCOMP],[OL_ESIC],[OL CTC],[OL Gross],[SR_BASIC],[SR_DA],[SR_EMPF],[SR_EMPCOMP],[SR_ESIC],[SR CTC],[SR Gross],[SR effective date] From [" + SheetName + "]";
                oda.SelectCommand = cmdExcel;
                oda.Fill(dataTable);
                connExcel.Close();
                log.Info("Excel file readed successfully and converted to data table.");
            }
            catch (Exception ex)
            {
                 exmsg = ex.Message.ToString();
                if (connExcel != null && connExcel.State != ConnectionState.Closed)
                    connExcel.Close();
                oda.Dispose();
                dataTable = null;
                log.Error("Error in getting data from Excel Sheet. " + ex.Message);
            }
            finally
            {
                string mm = exmsg;
                //if (System.IO.File.Exists(FilePath))
                //{
                //    System.IO.File.Delete(FilePath);
                //    log.Info("Excel file deleted from ExcelFiles folder");
                //}
                if (connExcel != null && cmdExcel != null && connExcel != null)
                {
                    oda.Dispose();
                    cmdExcel.Dispose();
                    connExcel.Dispose();
                }
            }
            return dataTable;
        }

        private static string GenerateXML(List<E_EXCEL_INFO> lstObj)
        {
            log.Info("generic list to xml conversion.");
            string xml = string.Empty;
            string result = string.Empty;
            try
            {

                XElement xmlElements = new XElement("TEAM_LEASE",
                                 from entity in lstObj
                                 select new XElement("TEAM_LEASE",
                                              new XAttribute("CLIENT_ID", string.IsNullOrEmpty(entity.CLIENT_ID) == true ? "" : entity.CLIENT_ID),
                                              new XAttribute("CLIENT_NAME", string.IsNullOrEmpty(entity.CLIENT_NAME) == true ? "" : entity.CLIENT_NAME),
                                              new XAttribute("EMPLOYEE_NO", string.IsNullOrEmpty(entity.EMPLOYEE_NO) == true ? "" : entity.EMPLOYEE_NO),
                                              new XAttribute("ASSOCIATE_NAME", string.IsNullOrEmpty(entity.ASSOCIATE_NAME) == true ? "" : entity.ASSOCIATE_NAME),
                                              new XAttribute("GENDER", string.IsNullOrEmpty(entity.GENDER) == true ? "" : entity.GENDER),
                                              new XAttribute("DOB", string.IsNullOrEmpty(entity._DOB) == true ? "" : entity._DOB),
                                              new XAttribute("DESIGNATION", string.IsNullOrEmpty(entity.DESIGNATION) == true ? "" : entity.DESIGNATION),
                                              new XAttribute("INSURANCE_COMPANY_NAME", string.IsNullOrEmpty(entity.INSURANCE_COMPANY_NAME) == true ? "" : entity.INSURANCE_COMPANY_NAME),
                                              new XAttribute("INSURANCE_POLICY_TYPE", string.IsNullOrEmpty(entity.INSURANCE_POLICY_TYPE) == true ? "" : entity.INSURANCE_POLICY_TYPE),
                                              new XAttribute("INSURANCE_POLICY", string.IsNullOrEmpty(entity.INSURANCE_POLICY) == true ? "" : entity.INSURANCE_POLICY),
                                              new XAttribute("SUM_INSURED", string.IsNullOrEmpty(entity._SUM_INSURED) == true ? "" : entity._SUM_INSURED),
                                              new XAttribute("SUM_INSURED1", string.IsNullOrEmpty(entity._SUM_INSURED1) == true ? "" : entity._SUM_INSURED1),
                                              new XAttribute("DOJ", string.IsNullOrEmpty(entity._DOJ) == true ? "" : entity._DOJ),
                                              new XAttribute("ACTUAL_DOL", string.IsNullOrEmpty(entity.ACTUAL_DOL) == true ? "" : entity.ACTUAL_DOL),
                                              new XAttribute("ACTIVE_STATUS", string.IsNullOrEmpty(entity.ACTIVE_STATUS) == true ? "" : entity.ACTIVE_STATUS),
                                              new XAttribute("STOPPAY_STATUS", string.IsNullOrEmpty(entity.STOPPAY_STATUS) == true ? "" : entity.STOPPAY_STATUS),
                                              new XAttribute("OL_BASIC", string.IsNullOrEmpty(entity.OL_BASIC) == true ? "0" : entity.OL_BASIC),
                                              new XAttribute("OL_DA", string.IsNullOrEmpty(entity.OL_DA) == true ? "0" : entity.OL_DA),
                                              new XAttribute("OL_EMPF", string.IsNullOrEmpty(entity.OL_EMPF) == true ? "0" : entity.OL_EMPF),
                                              new XAttribute("OL_EMPCOMP", string.IsNullOrEmpty(entity.OL_EMPCOMP) == true ? "0" : entity.OL_EMPCOMP),
                                              new XAttribute("OL_ESIC", string.IsNullOrEmpty(entity.OL_ESIC) == true ? "0" : entity.OL_ESIC),
                                              new XAttribute("OL_CTC", string.IsNullOrEmpty(entity.OL_CTC) == true ? "0" : entity.OL_CTC),
                                              new XAttribute("OL_GROSS", string.IsNullOrEmpty(entity.OL_GROSS) == true ? "0" : entity.OL_GROSS),
                                              new XAttribute("SR_BASIC", string.IsNullOrEmpty(entity.SR_BASIC) == true ? "0" : entity.SR_BASIC),
                                              new XAttribute("SR_DA", string.IsNullOrEmpty(entity.SR_DA) == true ? "" : entity.SR_DA),
                                              new XAttribute("SR_EMPF", string.IsNullOrEmpty(entity.SR_EMPF) == true ? "0" : entity.SR_EMPF),
                                              new XAttribute("SR_EMPCOMP", string.IsNullOrEmpty(entity.SR_EMPCOMP) == true ? "0" : entity.SR_EMPCOMP),
                                              new XAttribute("SR_ESIC", string.IsNullOrEmpty(entity.SR_ESIC) == true ? "0" : entity.SR_ESIC),
                                              new XAttribute("SR_CTC", string.IsNullOrEmpty(entity.SR_CTC) == true ? "0" : entity.SR_CTC),
                                              new XAttribute("SR_GROSS", string.IsNullOrEmpty(entity.SR_GROSS) == true ? "0" : entity.SR_GROSS),
                                              new XAttribute("SR_EFFECTIVE_DATE", string.IsNullOrEmpty(entity.SR_EFFECTIVE_DATE) == true ? "" : entity.SR_EFFECTIVE_DATE)
                                            ));

                xml = xmlElements.ToString();
                result = XDocument.Parse(xml).ToString(SaveOptions.DisableFormatting);
                log.Info("generic list to xml conversion finished.");
            }
            catch (Exception ex)
            {
                xml = string.Empty;
                log.Error("Error while generating XML from generic list. " + ex.Message);
            }
            return result;
        }

        public static long InsertXML(string Filepath,List<E_EXCEL_INFO> objExcel, ref string errMessage)
        {
            log.Info("InsertXML method called:");
            long retValue = 0;
            string compId = System.Configuration.ConfigurationManager.AppSettings["compId"].ToString();
            E_EXCEL_INFO obj = new E_EXCEL_INFO();
            //Commented by annapurna
            //string enrollFrom = "2018-07-27";
            //string enrollTo = "2018-07-27";
            //end annapurna

            //Passing Current Date
            string enrollFrom = DateTime.Now.ToString("yyyy-MM-dd");
            string enrollTo = DateTime.Now.ToString("yyyy-MM-dd");

            try
            {
                //objExcel = FilterList(objExcel, ref errMessage, compId);
                if (string.IsNullOrEmpty(errMessage))
                {
                    log.Info("No errors in excel.");
                    if (objExcel != null && objExcel.Count != 0)
                    {
                        string xml = GenerateXML(objExcel);
                        log.Info("xml string sent to DataLayer.");
                        retValue = InsertExcelTeamLease(Filepath,Convert.ToInt32(compId), enrollFrom, enrollTo, xml);
                    }
                }
                else
                    retValue = 0;
            }
            catch (Exception ex)
            {
                retValue = 0;
                log.Error("Error in InsertXML. " + ex.Message);
            }
            return retValue;
        }

        public static SqlConnection CreateSqlConnection(string connectionString)
        {
            SqlConnection sqlCon = null;
            try
            {
                sqlCon = string.IsNullOrEmpty(connectionString) ? null : new SqlConnection(connectionString);
            }
            catch (Exception ex)
            {
                sqlCon = null;
                log.Error("Error occured while creating connection " + ex.Message);
            }
            return sqlCon;
        }
        public static string GetConnectionString()
        {
            string connectionString = string.Empty;
            try
            {
                connectionString = System.Configuration.ConfigurationManager.AppSettings["conString"];
            }
            catch (Exception ex)
            {
                log.Error("GetConnectionString " + ex.Message);
                connectionString = null;
            }
            return connectionString;
        }


        public static  void SendMail(string Filepath,DataSet ds)
        {
            try
            {
                string userName = System.Configuration.ConfigurationManager.AppSettings["userName"].ToString();
                string password = System.Configuration.ConfigurationManager.AppSettings["password"].ToString();
                string HostName = System.Configuration.ConfigurationManager.AppSettings["HostName"].ToString();
                string mailFrom = System.Configuration.ConfigurationManager.AppSettings["mailFrom"].ToString();
                string mailTo = System.Configuration.ConfigurationManager.AppSettings["mailTo"].ToString();
                //string mailCC = System.Configuration.ConfigurationManager.AppSettings["mailCC"].ToString();
                string FileName = System.IO.Path.GetFileName(Filepath);
                string textBody = "Hi Team,<br>Please find below details for FileName-"+FileName+"<br><br>";
                textBody += " <table border=" + 1 + " cellpadding=" + 0 + " cellspacing=" + 0 + " width = " + 600 + "><tr bgcolor='#4da6ff'><td><b>PROMOTION1</b></td> <td> <b> PROMOTIONSI</b> </td><td> <b> TRANSFER1</b> </td><td> <b> TRANSFERSI</b> </td><td> <b> DELETION1</b> </td><td> <b> DELETIONSI</b> </td><td> <b> ADDITION1</b> </td><td> <b> ADDITIONSI</b> </td><td> <b> READDITION</b> </td><td> <b> READDITIONSI</b> </td><td> <b> SUCCESS1</b> </td><td> <b> SUCCESSSI</b> </td><td> <b> FAILURE</b> </td><td></tr>";

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    textBody += "<tr><td>" + ds.Tables[0].Rows[i]["PROMOTION1"].ToString() + "</td><td> " + ds.Tables[0].Rows[i]["PROMOTIONSI"].ToString() + "</td><td> " + ds.Tables[0].Rows[i]["TRANSFER1"].ToString() + "</td><td> " + ds.Tables[0].Rows[i]["TRANSFERSI"].ToString() + "</td><td> " + ds.Tables[0].Rows[i]["DELETION1"].ToString() + "</td><td> " + ds.Tables[0].Rows[i]["DELETIONSI"].ToString() + "</td><td> " + ds.Tables[0].Rows[i]["ADDITION1"].ToString() + "</td><td> " + ds.Tables[0].Rows[i]["ADDITIONSI"].ToString() + "</td><td> " + ds.Tables[0].Rows[i]["READDITION"].ToString() + "</td><td> " + ds.Tables[0].Rows[i]["READDITIONSI"].ToString() + "</td><td> " + ds.Tables[0].Rows[i]["SUCCESS1"].ToString() + "</td><td> " + ds.Tables[0].Rows[i]["SUCCESSSI"].ToString() + "</td><td> " + ds.Tables[0].Rows[i]["FAILURE"].ToString() + "</td></tr>";
                }
                textBody += "</table>";

                string textBody1 = "<br<br><br>";
                textBody1 += " <table border=" + 1 + " cellpadding=" + 0 + " cellspacing=" + 0 + " width = " + 600 + "><tr bgcolor='#4da6ff'><td><b>EMPLOYEE_NO</b></td> <td> <b> ASSOCIATE_NAME</b> </td><td> <b> SUM_INSURED</b> </td><td> <b> SUM_INSURED1</b> </td></tr>";

                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                {
                   // textBody1 += "<tr><td>" + ds.Tables[1].Rows[i]["EMPCODE"].ToString() + "</td><td> " + ds.Tables[1].Rows[i]["EMPLOYEE_NAME"].ToString() + "</td><td> " + ds.Tables[1].Rows[i]["GMC_SUM_INSURED"].ToString() + "</td><td> " + ds.Tables[1].Rows[i]["GPA_SUM_INSURED"].ToString() + "</td> </tr>";
                    textBody1 += "<tr><td>" + ds.Tables[1].Rows[i]["EMPLOYEE_NO"].ToString() + "</td><td> " + ds.Tables[1].Rows[i]["ASSOCIATE_NAME"].ToString() + "</td><td> " + ds.Tables[1].Rows[i]["SUM_INSURED"].ToString() + "</td><td> " + ds.Tables[1].Rows[i]["SUM_INSURED1"].ToString() + "</td> </tr>";
                }
                textBody1 += "</table>";

                string MailBody=textBody+textBody1 + "<br>Thanks & Regards,<br>Rajender.";



                //textBody += "<br>Thanks & Regards<br>Rajender.";
                MailMessage mail = new MailMessage();
                System.Net.Mail.SmtpClient SmtpServer1 = new SmtpClient(HostName);

                mail.From = new MailAddress(mailFrom);
                //mail.To.Add(mailTo); //Commented by annapurna
                mail.To.Add("dothula.annapurna@isbsindia.in");
                //mail.CC.Add(mailCC);
                mail.Subject = "Success and Failure Details Data";
                mail.Body = MailBody;
                mail.IsBodyHtml = true;
                SmtpServer1.Port = 587;
                SmtpServer1.Credentials = new System.Net.NetworkCredential(userName, password);
                SmtpServer1.EnableSsl = true;


                SmtpServer1.Send(mail);
            }
            catch (Exception ex)
            {
                log.Error("Error in sending email. " + ex.Message);
            }

        }

        public static long InsertExcelTeamLease(string Filepath,int compId, string EnrollFrom, string EnrollTo, string xml)
        {
            log.Info("method called InsertExcelTeamLease");
            SqlConnection sqlCon = null;
            Int64 isInserted = 0;
            try
            {
                log.Info("xml string insertion started.");
                sqlCon = CreateSqlConnection(GetConnectionString());
                SqlCommand sqlCmd = new SqlCommand("SP_INSERT_TL_EXCEL_INSURED_PERSON", sqlCon);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@I_COMP_ID", SqlDbType.Int).Value = compId;
                sqlCmd.Parameters.Add("@I_ENROLL_FROM", SqlDbType.Date).Value = Convert.ToDateTime(EnrollFrom);
                sqlCmd.Parameters.Add("@I_ENROLL_TO", SqlDbType.Date).Value = Convert.ToDateTime(EnrollTo);
                sqlCmd.Parameters.Add("@I_XML", SqlDbType.Xml).Value = xml;
                sqlCmd.Parameters.Add("@I_Return_Value", SqlDbType.BigInt).Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@I_ERRORMSG", SqlDbType.VarChar,8000).Direction = ParameterDirection.Output;
                sqlCon.Open();
                log.Info("connection created successfully");
                SqlDataAdapter adr = new SqlDataAdapter(sqlCmd);
                DataSet ds = new DataSet();
                adr.SelectCommand = sqlCmd;
                adr.Fill(ds);
            
               
                log.Info("SP_INSERT_TL_EXCEL_INSURED_PERSON executed");
                isInserted = Convert.ToInt64(sqlCmd.Parameters["@I_Return_Value"].Value);
                string errormessage = sqlCmd.Parameters["@I_ERRORMSG"].Value.ToString();
                log.Info("Return value :" + isInserted);
                log.Info("xml string insertion status from DB." + isInserted);
                sqlCmd.Parameters.Clear();
                sqlCmd.Dispose();
                sqlCon.Close();
                if(isInserted>0)
                {
                    SendMail(Filepath,ds); //commented by annapurna
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                isInserted = 0;
                log.Error("Error occured in InsertExcelTeamLease. " + ex.Message);
                if (sqlCon != null)
                    sqlCon.Close();
            }
            return isInserted;
        }        

    }


}
