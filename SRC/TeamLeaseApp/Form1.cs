﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TeamLeaseApp
{
    public partial class Form1 : Form
    {
        private static readonly log4net.ILog _logger =
       log4net.LogManager.GetLogger(typeof(Form1));
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //string ftpURL = "ftp://192.168.100.123";           //Host URL or address of the FTP server
            //string UserName = "ftpuser";                 //User Name of the FTP server
            //string Password = "ftpuser@123";              //Password of the FTP server
            //string ftpDirectory = "/harddisk2/ftp_share";          //The directory in FTP server where the files are present
            //string _FileName = "Associate data for automation_Latest_ADD";// "Associate data for automation_10082018.xlsx";             //File name, which one will be downloaded
            //string _LocalDirectory = "D:\\Download";  //Local directory where the files will be downloaded
            // string folder = "/harddisk2/ftp_share";

            TLImportExcel obj = new TLImportExcel();
            //obj.DownloadFile(ftpURL, UserName, Password, ftpDirectory, _FileName, _LocalDirectory);
            obj.DownloadFile();

            //TLImportExcel obj = new TLImportExcel();
            //obj.DownloadFiles();

            //DirectoryListing

            //For Local file processing
            //TLImportExcel obj1 = new TLImportExcel();
            //obj1.ExcelInsertToDB();


        }
    }
}
