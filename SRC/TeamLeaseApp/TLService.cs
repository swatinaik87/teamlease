﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace TeamLeaseApp
{
    public partial class TLService : ServiceBase
    {
        /// To maintain the log information in a text file.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        string _ftpURL = System.Configuration.ConfigurationManager.AppSettings["_ftpURL"].ToString();
        string _UserName = System.Configuration.ConfigurationManager.AppSettings["_UserName"].ToString();
        string _Password = System.Configuration.ConfigurationManager.AppSettings["_Password"].ToString();
        string _ftpDirectory = System.Configuration.ConfigurationManager.AppSettings["_ftpDirectory"].ToString();
        string _FileName = System.Configuration.ConfigurationManager.AppSettings["_FileName"].ToString();
        string _LocalDirectory = System.Configuration.ConfigurationManager.AppSettings["_LocalDirectory"].ToString();

        private System.Threading.Timer Schedular;
        public TLService()
        {
            InitializeComponent();
        }
        //string[] args
        protected override void OnStart(string[] args)
        {
            this.WriteToFile("Service started {0}");
            this.ScheduleService();

            TLImportExcel obj = new TLImportExcel();
            // obj.DownloadFile(_ftpURL, _UserName, _Password, _ftpDirectory, _FileName, _LocalDirectory);
            obj.DownloadFiles();
        }

        protected override void OnStop()
        {
            this.WriteToFile("Service stopped {0}");
            this.Schedular.Dispose();
        }

        public void ScheduleService()
        {
            try
            {
                Schedular = new System.Threading.Timer(new TimerCallback(SchedularCallback));
                string mode = ConfigurationManager.AppSettings["Mode"].ToUpper();
                this.WriteToFile("Service Mode: " + mode + " {0}");

                //Set the Default Time.
                DateTime scheduledTime = DateTime.MinValue;

                if (mode == "DAILY")
                {
                    //Get the Scheduled Time from AppSettings.
                    scheduledTime = DateTime.Parse(System.Configuration.ConfigurationManager.AppSettings["ScheduledTime"]);
                    if (DateTime.Now > scheduledTime)
                    {
                        //If Scheduled Time is passed set Schedule for the next day.
                        scheduledTime = scheduledTime.AddDays(1);
                    }
                }

                //if (mode.ToUpper() == "INTERVAL")
                //{
                //    //Get the Interval in Minutes from AppSettings.
                //    int intervalMinutes = Convert.ToInt32(ConfigurationManager.AppSettings["IntervalMinutes"]);

                //    //Set the Scheduled Time by adding the Interval to Current Time.
                //    scheduledTime = DateTime.Now.AddMinutes(intervalMinutes);
                //    if (DateTime.Now > scheduledTime)
                //    {
                //        //If Scheduled Time is passed set Schedule for the next Interval.
                //        scheduledTime = scheduledTime.AddMinutes(intervalMinutes);
                //    }
                //}

                TimeSpan timeSpan = scheduledTime.Subtract(DateTime.Now);
                string schedule = string.Format("{0} day(s) {1} hour(s) {2} minute(s) {3} seconds(s)", timeSpan.Days, timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);

                this.WriteToFile("Service scheduled to run after: " + schedule + " {0}");

                //Get the difference in Minutes between the Scheduled and Current Time.
                int dueTime = Convert.ToInt32(timeSpan.TotalMilliseconds);

                //Change the Timer's Due Time.
                Schedular.Change(dueTime, Timeout.Infinite);
            }
            catch (Exception ex)
            {
                WriteToFile("Service Error on: {0} " + ex.Message + ex.StackTrace);

                //Stop the Windows Service.
                using (System.ServiceProcess.ServiceController serviceController = new System.ServiceProcess.ServiceController("SimpleService"))
                {
                    serviceController.Stop();
                }
            }
        }

        private void SchedularCallback(object e)
        {
            this.WriteToFile("Service Log: {0}");
            this.ScheduleService();
        }

        private void WriteToFile(string text)
        {
            string path = System.Configuration.ConfigurationManager.AppSettings["ServiceLog"].ToString();
            //string path = "C:\\ServiceLog.txt";
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(string.Format(text, DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")));
                writer.Close();
            }
        }
    }
}


   
