﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamLeaseApp
{
    public class E_EXCEL_INFO : E_BASE
    {
        public string _CLIENT_ID;
        public string Message11;
        public string CLIENT_ID
        {
            get { return _CLIENT_ID; }
            set { _CLIENT_ID = IllegalMand(value, "_CLIENT_ID", 50); }
        }

        public string _CLIENT_NAME;
        public string CLIENT_NAME
        {
            get { return _CLIENT_NAME; }
            set { _CLIENT_NAME = IllegalMand(value, "CLIENT_NAME", 100); }
        }

        public string _EMPLOYEE_NO;
        public string EMPLOYEE_NO
        {
            get { return _EMPLOYEE_NO; }
            set { _EMPLOYEE_NO = IllegalMand(value, "EMPLOYEE_NO", 5); }
        }

        public string _ASSOCIATE_NAME;
        public string ASSOCIATE_NAME
        {
            get { return _ASSOCIATE_NAME; }
            set { _ASSOCIATE_NAME = IllegalMand(value, "ASSOCIATE_NAME", 100); }
        }

        public string _GENDER;
        public string GENDER
        {
            get { return _GENDER; }
            set
            {
                string gen = IllegalMand(value, "GENDER", 6);
                if (!string.IsNullOrEmpty(gen))
                {
                    if (gen.ToUpper() == "MALE" || gen.ToUpper() == "FEMALE")
                        _GENDER = gen;
                    else
                        Message.Append("GENDER should be Male or Female.RowNo" + RowNo + "<br>");
                }
                else
                    _GENDER = string.Empty;
            }
        }

        //public string DOB { get; set; }
        public string _DOB;
        public string DOB
        {
            get { return _DOB; }
            set { _DOB = InvalidDate(value, "DOB"); }
        }

        public string DESIGNATION { get; set; }
        
        public string INSURANCE_COMPANY_NAME { get; set; }
        
        public string INSURANCE_POLICY_TYPE { get; set; }
        
        public string INSURANCE_POLICY { get; set; }
        
        public string _SUM_INSURED;
        public string SUM_INSURED
        {
            get { return _SUM_INSURED; }
            set { _SUM_INSURED = NumericIllegalMand(value, "SUM_INSURED"); }
        }

        public string _SUM_INSURED1;
        public string SUM_INSURED1
        {
            get { return _SUM_INSURED; }
            set { _SUM_INSURED1 = NumericIllegalMand(value, "SUM_INSURED1"); }
        }

        //public string DOJ { get; set; }

        public string _DOJ;
        public string DOJ
        {
            get { return _DOJ; }
            set { _DOJ = InvalidDate(value, "DOJ"); }
        }
        //public string ACTUAL_DOL { get; set; }

        public string _ACTUAL_DOL;
        public string ACTUAL_DOL
        {
            get { return _ACTUAL_DOL; }
            set
            {
                if (value == "NULL" || value == "null")//Added by annapurna
                    value = "1/1/1900 12:00:00 AM";//Added by annapurna
                _ACTUAL_DOL = SetISOFormat(value, "ACTUAL_DOL");
            }
        }
        public string ACTIVE_STATUS { get; set; }
       
        public string STOPPAY_STATUS { get; set; }

        //NumericIllegal
        //public string OL_BASIC { get; set; }

        public string _OL_BASIC;
        public string OL_BASIC
        {
            get { return _OL_BASIC; }
            set { _OL_BASIC = NumericIllegal(value, "OL_BASIC"); }
        }
        //public string OL_DA { get; set; }

        public string _OL_DA;
        public string OL_DA
        {
            get { return _OL_DA; }
            set { _OL_DA = NumericIllegal(value, "OL_DA"); }
        }

       // public string OL_EMPF { get; set; }

        public string _OL_EMPF;
        public string OL_EMPF
        {
            get { return _OL_EMPF; }
            set { _OL_EMPF = NumericIllegal(value, "OL_EMPF"); }
        }
       // public string OL_EMPCOMP { get; set; }

        public string _OL_EMPCOMP;
        public string OL_EMPCOMP
        {
            get { return _OL_EMPCOMP; }
            set { _OL_EMPCOMP = NumericIllegal(value, "OL_EMPCOMP"); }
        }

        //public string OL_ESIC { get; set; }

        public string _OL_ESIC;
        public string OL_ESIC
        {
            get { return _OL_ESIC; }
            set { _OL_ESIC = NumericIllegal(value, "OL_ESIC"); }
        }
        //public string OL_CTC { get; set; }

        public string _OL_CTC;
        public string OL_CTC
        {
            get { return _OL_CTC; }
            set { _OL_CTC = NumericIllegal(value, "OL_CTC"); }
        }
        //public string OL_GROSS { get; set; }

        public string _OL_GROSS;
        public string OL_GROSS
        {
            get { return _OL_GROSS; }
            set { _OL_GROSS = NumericIllegal(value, "OL_GROSS"); }
        }

       // public string SR_BASIC { get; set; }

        public string _SR_BASIC;
        public string SR_BASIC
        {
            get { return _SR_BASIC; }
            set { _SR_BASIC = NumericIllegal(value, "SR_BASIC"); }
        }
        //public string SR_DA { get; set; }

        public string _SR_DA;
        public string SR_DA
        {
            get { return _SR_DA; }
            set { _SR_DA = NumericIllegal(value, "SR_DA"); }
        }
        //public string SR_EMPF { get; set; }

        public string _SR_EMPF;
        public string SR_EMPF
        {
            get { return _SR_EMPF; }
            set { _SR_EMPF = NumericIllegal(value, "SR_EMPF"); }
        }
       // public string SR_EMPCOMP { get; set; }

        public string _SR_EMPCOMP;
        public string SR_EMPCOMP
        {
            get { return _SR_EMPCOMP; }
            set { _SR_EMPCOMP = NumericIllegal(value, "SR_EMPCOMP"); }
        }
        //public string SR_ESIC { get; set; }

        public string _SR_ESIC;
        public string SR_ESIC
        {
            get { return _SR_ESIC; }
            set { _SR_ESIC = NumericIllegal(value, "SR_ESIC"); }
        }
        //public string SR_CTC;

        public string _SR_CTC;
        public string SR_CTC
        {
            get { return _SR_CTC; }
            set { _SR_CTC = NumericIllegal(value, "SR_CTC"); }
        }

       // public string SR_GROSS;

        public string _SR_GROSS;
        public string SR_GROSS
        {
            get { return _SR_GROSS; }
            set { _SR_GROSS = NumericIllegal(value, "SR_GROSS"); }
        }

        // public string SR_EFFECTIVE_DATE { get; set; }

        public string _SR_EFFECTIVE_DATE;
        public string SR_EFFECTIVE_DATE
        {
            get { return _SR_EFFECTIVE_DATE; }
            set { _SR_EFFECTIVE_DATE = SetISOFormat(value, "SR_EFFECTIVE_DATE"); }
        }

        //public string EMP_CODE { get; set; }
        //public string EMP_NAME { get; set; }
        //public string GENDER { get; set; }
        //public string DOB { get; set; }
        //public string CLIENT_ID { get; set; }
        //public string CLIENT_NAME { get; set; }
        //public string DOJ { get; set; }
        //public string POLICY_DOJ { get; set; }
        //public string POLICY_TYPE { get; set; }
        //public string GMC_SUM_INSURED { get; set; }
        //public string GPA_SUM_INSURED { get; set; }
        //public string ADDITION_REMARKS { get; set; }
        // public string DOL { get; set; }
        //public string POLICY_DOL { get; set; }
        //public string GROSS_SALARY { get; set; }
        //public string DELETION_MONTH { get; set; }
        //public int RowNo { get; set; }


    }

    public class E_EXCEL_INFO_New : E_BASE
    {
    
        public string _EMPLOYEE_NO;
        public string _ASSOCIATE_NAME;
        public string _RELATION;
        public string _GENDER;
        public string _DOB;
        public string _CLIENT_ID;
        public string _CLIENT_NAME;
        public string _DOJ;
        public string _POLICYTYPE;
        public string _GPA_SUM_INSURED;
        public string _GMC_SUM_INSURED;
        public string _POLICY_DOJ;
        public string _REMARKS;
        public string _GROSS;
    
     
       
      
        public string EMPLOYEE_NO
        {
            get { return _EMPLOYEE_NO; }
            set { _EMPLOYEE_NO = IllegalMand(value, "EMPLOYEE_NO", 50); }
        }
      
        public string ASSOCIATE_NAME
        {
            get { return _ASSOCIATE_NAME; }
            set { _ASSOCIATE_NAME = IllegalMand(value, "ASSOCIATE_NAME", 100); }
        }
       
        public string RELATION
        {
            get { return _RELATION; }
            set { _RELATION = IllegalMand(value, "RELATION", 100); }
        }
    
        public string GENDER
        {
            get { return _GENDER; }
            set
            {
                string gen = IllegalMand(value, "GENDER", 6);
                if (!string.IsNullOrEmpty(gen))
                {
                    if (gen.ToUpper() == "MALE" || gen.ToUpper() == "FEMALE")
                        _GENDER = gen;
                    else
                        Message.Append("GENDER should be Male or Female.RowNo" + RowNo + "<br>");
                }
                else
                    _GENDER = string.Empty;
            }
        }
        
        public string DOB
        {
            get { return _DOB; }
            set { _DOB = InvalidDate(value, "DOB"); }
        }
     
        public string CLIENT_ID
        {
            get { return _CLIENT_ID; }
            set { _CLIENT_ID = IllegalMand(value, "_CLIENT_ID", 50); }
        }

        public string CLIENT_NAME
        {
            get { return _CLIENT_NAME; }
            set { _CLIENT_NAME = IllegalMand(value, "CLIENT_NAME", 100); }
        }
  
        public string DOJ
        {
            get { return _DOJ; }
            set { _DOJ = InvalidDate(value, "DOJ"); }
        }
       
        public string POLICYTYPE
        {
            get { return _POLICYTYPE; }
            set { _POLICYTYPE = IllegalMand(value, "POLICYTYPE", 100); }
        }
      
        public string GMC_SUM_INSURED
        {
            get { return _GMC_SUM_INSURED; }
            set { _GMC_SUM_INSURED = NumericIllegalMand(value, "GMC_SUM_INSURED"); }
        }

      
        public string GPA_SUM_INSURED
        {
            get { return _GPA_SUM_INSURED; }
            set { _GPA_SUM_INSURED = NumericIllegalMand(value, "GPA_SUM_INSURED"); }
        }
    
        public string POLICY_DOJ
        {
            get { return _POLICY_DOJ; }
            set { _POLICY_DOJ = InvalidDate(value, "POLICY_DOJ"); }
        }
   
        public string REMARKS
        {
            get { return _REMARKS; }
            set { _REMARKS = IllegalMand(value, "REMARKS", 100); }
        }
       
        public string GROSS
        {
            get { return _GROSS; }
            set { _GROSS = NumericIllegalMand(value, "GROSS"); }
        }
      
    }
}
